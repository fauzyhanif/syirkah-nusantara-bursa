<table class="table table-striped table-bordered table-sm">
    <thead class="bg-info">
        <th>Nomor Topup</th>
        <th width="30%">Tanggal</th>
        <th>Status</th>
        <th>Nilai</th>
    </thead>
    <tbody>
        @foreach ($topups as $topup)
            <tr>
                <td>
                    <a href="{{ url('member/topup/detail', $topup->topup_id) }}">
                        {{ $topup->topup_number }}
                    </a>
                </td>
                <td>{{ GeneralHelper::konversiTgl($topup->topup_date) }}</td>
                <td>
                    @if (date('Y-m-d H:i') > $topup->topup_expired && $topup->topup_status_id == '0')
                        <span class="text-danger">Expired</span>
                    @else
                        {{ $topup->topup_status->topup_status_name }}
                    @endif
                </td>
                <td>{{ GeneralHelper::rupiah($topup->topup_nominal) }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

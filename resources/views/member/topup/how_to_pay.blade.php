@extends('layouts.member')

@section('content')

<input type="hidden" name="topup_expired" value="{{ $topup->topup_expired }}">

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"> Cara Top Up</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Top Up</a></li>
                    <li class="breadcrumb-item active">Cara Bayar</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Nomor Topup <span class="font-weight-bold">{{ $topup->topup_number }}</span>
                        </h5>
                    </div>
                    <div class="card-body">
                        <div class="row">

                            @if ($topup->topup_status_id == '0' && date('Y-m-d H:i') > $topup->topup_expired)
                                <div class="col-lg-12 border rounded mb-4">
                                    <div class="wrapp m-2 text-center">
                                        <h5 class="font-weight-bold text-danger">
                                            Waktu pembayaran sudah melebihi batas!
                                        </h5>

                                        <p class="text-secondary">
                                            Silahkan buat topup baru lagi.
                                        </p>

                                        <a href="{{ route('member.topup.form_store') }}" class="btn btn-sm btn-primary font-weight-bold">
                                            TOPUP
                                        </a>
                                    </div>
                                </div>
                            @elseif ($topup->topup_status_id == '0' && date('Y-m-d H:i') < $topup->topup_expired)
                                <div class="col-lg-12 border rounded">
                                    <div class="wrapp m-2 text-center">
                                        <p class="text-secondary">
                                            Bayarkan sebelum :
                                        </p>

                                        <h5 class="text-primary font-weight-bold" style="margin-top: -15px;" id="countdown">
                                            -
                                        </h5>

                                        <p class="text-secondary mt-4">
                                            Batas pembayaran :
                                        </p>

                                        <h5 class="text-primary font-weight-bold" style="margin-top: -15px;">
                                            {{ GeneralHelper::konversiTgl(substr($topup->topup_expired, 0, 10)) }} Jam {{ substr($topup->topup_expired, -5) }}
                                        </h5>
                                    </div>
                                </div>
                            @elseif ($topup->topup_status_id == '3')
                                <div class="col-lg-12 border rounded">
                                    <div class="wrapp m-2 text-center">
                                        <h5 class="text-primary font-weight-bold">
                                            Sedang Proses
                                        </h5>
                                        <p class="text-secondary">
                                            Isi saldo sedang diproses
                                        </p>
                                    </div>
                                </div>
                            @elseif ($topup->topup_status_id == '1')
                                <div class="col-lg-12 border rounded">
                                    <div class="wrapp m-2 text-center">
                                        <h5 class="text-success font-weight-bold">
                                            Pengisian saldo berhasil diproses
                                        </h5>
                                        <p class="text-secondary">
                                            Dana berhasil ditambahkan ke dalam saldo anda
                                        </p>
                                    </div>
                                </div>
                            @elseif ($topup->topup_status_id == '2')
                                <div class="col-lg-12 border rounded">
                                    <div class="wrapp m-2 text-center">
                                        <h5 class="text-danger font-weight-bold">
                                            Pengisian saldo gagal diproses
                                        </h5>
                                        <p class="text-secondary">
                                            Dana gagal ditambahkan ke dalam saldo anda
                                        </p>
                                    </div>
                                </div>
                            @endif

                            <div class="col-lg-12 border rounded mt-4">
                                <div class="wrapp m-2">
                                    <p class="font-weight-bold">
                                        Detail Topup
                                    </p>

                                    <div class="row">
                                        <div class="col-4 text-secondary">No Topup</div>
                                        <div class="col-8 text-right">{{ $topup->topup_number }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4 text-secondary">Tgl Topup</div>
                                        <div class="col-8 text-right">{{ GeneralHelper::konversiTgl($topup->topup_date) }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4 text-secondary">Jenis Topup</div>
                                        <div class="col-8 text-right">{{ $topup->topup_type->saving_type_name }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4 text-secondary">Bank Anda</div>
                                        <div class="col-8 text-right">{{ $topup->topup_member_bank . " " .$topup->topup_member_rek_number }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4 text-secondary">Pembayaran</div>
                                        <div class="col-8 text-right">Rp {{ GeneralHelper::rupiah($topup->topup_nominal - $topup->topup_unique_code) }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4 text-secondary">Nomor Unik</div>
                                        <div class="col-8 text-right">Rp {{ GeneralHelper::rupiah($topup->topup_unique_code) }}</div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-6">Jumlah Transfer</div>
                                        <div class="col-6 text-right font-weight-bold">Rp {{ GeneralHelper::rupiah($topup->topup_nominal) }}</div>
                                    </div>
                                </div>
                            </div>

                            @if (date('Y-m-d H:i') < $topup->topup_expired)
                                <div class="col-lg-12 border rounded mt-4">
                                    <div class="wrapp m-2 text-center">
                                        <p class="text-secondary">
                                            Transfer ke :
                                        </p>

                                        <h5 class="text-secondary font-weight-bold" style="margin-top: -15px;">
                                            Bank BRI
                                        </h5>

                                        <h5 class="text-primary font-weight-bold" style="margin-top: -5px">
                                            742701002047538
                                        </h5>

                                        <h5 class="text-secondary font-weight-bold" style="margin-top: -5px">
                                            An Taufik Hidayat
                                        </h5>

                                    </div>
                                </div>

                                <div class="col-lg-12 border rounded mt-4">
                                    <div class="wrapp m-2">
                                        <p class="font-weight-bold">
                                            Penting
                                        </p>

                                        <p>
                                            Mohon transfer <span class="text-primary">sesuai jumlah tagihan</span>
                                            yaitu sebesar <span class="text-primary">Rp {{ GeneralHelper::rupiah($topup->topup_nominal) }}</span>.
                                            Karna sistem kami tidak bisa mengenali pembayaran Anda jika jumlahnya tidak sesuai.
                                        </p>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if ($topup->topup_status_id == '0' && date('Y-m-d H:i') < $topup->topup_expired)
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <a href="{{ url('member/topup/transfered', $topup->topup_id) }}" class="btn btn-sm btn-primary btn-block">SAYA SUDAH TRANSFER</a>
                </div>
            </div>
        @endif

        <div class="row justify-content-center mt-2 pb-4">
            <div class="col-lg-8">
                <div class="row">
                    <div class="col-lg-3 mb-sm-2">
                        <a href="{{ route('member.dashboard') }}" class="btn btn-sm btn-outline-primary btn-block">KEMBALI KE DASHBOARD</a>
                    </div>
                    @if ($topup->topup_status_id == '0' && date('Y-m-d H:i') < $topup->topup_expired)
                        <div class="col-lg-3 mb-sm-2">
                            <a href="{{ url('member/topup/form_update', $topup->topup_id) }}" class="btn btn-sm btn-outline-primary btn-block">UBAH TOPUP</a>
                        </div>
                        <div class="col-lg-3 mb-sm-2">
                            <a href="{{ url('member/topup/delete', $topup->topup_id) }}" class="btn btn-sm btn-outline-danger btn-block">HAPUS TOPUP</a>
                        </div>
                    @endif
                    <div class="col-lg-3 mb-sm-2">
                        <a href="{{ route('member.topup.form_store') }}" class="btn btn-sm btn-outline-primary btn-block">TOPUP LAGI</a>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

<script>
    var topup_expired = $('input[name="topup_expired"]').val();

    // Set the date we're counting down to
    var countDownDate = new Date(topup_expired).getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

    // Get today's date and time
    var now = new Date().getTime();

    // Find the distance between now and the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id="demo"
    document.getElementById("countdown").innerHTML = hours + " Jam : " + minutes + " Menit : " + seconds + " Detik";

    // If the count down is over, write some text
    if (distance < 0) { clearInterval(x); document.getElementById("demo").innerHTML="EXPIRED" ; } }, 1000);
</script>
@endsection

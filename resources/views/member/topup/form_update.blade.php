@extends('layouts.member')

@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-12">
                <h1 class="m-0 text-dark"> Topup</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Form Update Topup
                        </h5>
                    </div>
                    <div class="card-body">
                        <form action="{{ url('member/topup/update', $topup->topup_id) }}" method="POST" data-remote>
                            <div class="form-group">
                                <label>Nominal <span class="text-red">*</span></label>
                                <input type="text" name="topup_nominal" required class="form-control money" value="{{ $topup->topup_nominal }}">
                            </div>

                            <div class="form-group">
                                <label>Jenis Tabungan <span class="text-red">*</span></label>
                                <select name="saving_type_id" required class="form-control">
                                    @foreach ($saving_types as $saving_type)
                                        <option value="{{ $saving_type->saving_type_id }}" {{ ($topup->saving_type_id == $saving_type->saving_type_id) ? 'selected' : '' }}>{{ $saving_type->saving_type_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Bank <span class="text-red">*</span></label>
                                <select name="topup_member_bank" class="form-control">
                                    <option {{ ($topup->topup_member_bank == 'BRI') ? 'selected' : '' }}>BRI</option>
                                    <option {{ ($topup->topup_member_bank == 'BNI') ? 'selected' : '' }}>BNI</option>
                                    <option {{ ($topup->topup_member_bank == 'BCA') ? 'selected' : '' }}>BCA</option>
                                    <option {{ ($topup->topup_member_bank == 'MANDIRI') ? 'selected' : '' }}>MANDIRI</option>
                                    <option {{ ($topup->topup_member_bank == 'BSI') ? 'selected' : '' }}>BSI</option>
                                    <option {{ ($topup->topup_member_bank == 'PERMATA') ? 'selected' : '' }}>PERMATA</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nomor Rekening <span class="text-red">*</span></label>
                                <input type="text" name="topup_member_rek_number" class="form-control" value="{{ $topup->topup_member_rek_number }}">
                            </div>

                            <button type="submit" class="btn btn-primary btn-block btn-sm mt-2">TOPUP</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

<script>
$(function() {
    $('.money').mask('000.000.000.000.000', {reverse: true});
});

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();

        // cek nominal minimal 1.000.000
        var nominal = $('input[name="topup_nominal"]').val().replace(/\./g, '');

        if (nominal >= 1000000) {
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.status == 'success') {
                        toastr.success(res.text);
                        // redirect to how to pay
                        var base_url = {!! json_encode(url('/member/topup/how_to_pay/')) !!};
                        var base_url = base_url + "/" + res.topup_id;
                        window.location.href = base_url;
                    } else {
                        toastr.error(res.text);
                    }
                }
            });
        } else {
            toastr.error("Minimal Topup Rp 1.000.000")
        }
    });
})();
</script>
@endsection

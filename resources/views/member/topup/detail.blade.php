@extends('layouts.member')

@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"> Detail Top Up</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Top Up</a></li>
                    <li class="breadcrumb-item active">Detail</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                @if ($topup->topup_status_id == '0' && date('Y-m-d H:i') < $topup->topup_expired)
                    <div class="alert alert-info">
                        <b>Masih menunggu verifikasi dari admin.</b>
                    </div>
                @elseif ($topup->topup_status_id == '0' && date('Y-m-d H:i') > $topup->topup_expired)
                    <div class="alert alert-danger">
                        <b>Mohon maaf topup tidak bisa dilanjutkan, waktu pembayaran sudah melebihi batas.</b>
                    </div>
                @elseif ($topup->topup_status_id == '1')
                    <div class="alert alert-success">
                        <b>Topup berhasil diverifikasi!</b>
                    </div>
                @elseif ($topup->topup_status_id == '2')
                    <div class="alert alert-success">
                        <b>Topup gagal diverifikasi!</b>
                    </div>
                @endif
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="card shadow-none">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3 text-secondary">Tanggal</div>
                            <div class="col-lg-8">{{ GeneralHelper::konversiTgl($topup->topup_date) }}</div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 text-secondary">No Topup</div>
                            <div class="col-lg-8">{{ $topup->topup_number }}</div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 text-secondary">Jenis Topup</div>
                            <div class="col-lg-8">{{ $topup->topup_type->saving_type_name }}</div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 text-secondary">Nominal</div>
                            <div class="col-lg-8">{{ GeneralHelper::rupiah($topup->topup_nominal) }}</div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 text-secondary">Bank</div>
                            <div class="col-lg-8">{{ $topup->topup_member_rek_number }}</div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    @if ($topup->topup_status_id == '0' && date('Y-m-d H:i') < $topup->topup_expired)
                        <div class="col-lg-3 mb-sm-2">
                            <a href="{{ url('member/topup/how_to_pay', $topup->topup_id) }}" class="btn btn-sm btn-success btn-block">CARA TOPUP</a>
                        </div>
                        <div class="col-lg-3 mb-sm-2">
                            <a href="{{ url('member/topup/form_update', $topup->topup_id) }}" class="btn btn-sm btn-primary btn-block">UBAH TOPUP</a>
                        </div>
                        <div class="col-lg-3 mb-sm-2">
                            <a href="{{ url('member/topup/delete', $topup->topup_id) }}" class="btn btn-sm btn-danger btn-block">HAPUS TOPUP</a>
                        </div>
                    @endif
                    <div class="col-lg-3">
                        <a href="{{ route('member.dashboard') }}" class="btn btn-sm btn-outline-primary btn-block">KEMBALI KE DASHBOARD</a>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

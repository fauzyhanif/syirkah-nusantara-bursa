@extends('layouts.member')

@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-12">
                <h1 class="m-0 text-dark"> Profile</h1>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container">

        <a href="{{ route('member.profile_form') }}" class="btn btn-primary btn-sm mb-3">PERBAHARUI DATA PRIBADI</a>

        @if ($member->member_nik == '')
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-warning">
                        Data pribadi anda belum lengkap, silahkan lengkapi data anda dengan benar.

                        <br>
                        <a href="{{ route('member.profile_form') }}" class="btn btn-xs btn-primary mt-2">
                            LENGKAPI DATA
                        </a>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-lg-7">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Data Pribadi Anda
                        </h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                Nama Lengkap
                            </div>
                            <div class="col-lg-8 col-md-12 font-weight-bold">
                                : {{ $member->member_name }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                Nomor Telepon
                            </div>
                            <div class="col-lg-8 col-md-12 font-weight-bold">
                                : {{ $member->member_phone }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                NIK (Nomor Induk KTP)
                            </div>
                            <div class="col-lg-8 col-md-12 font-weight-bold">
                                : {{ $member->member_nik }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                Nomor NPWP
                            </div>
                            <div class="col-lg-8 col-md-12 font-weight-bold">
                                : {{ $member->member_npwp }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                Penghasilan Pertahun
                            </div>
                            <div class="col-lg-8 col-md-12 font-weight-bold">
                                : {{ GeneralHelper::rupiah($member->member_income) }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                Pengalaman Investasi
                            </div>
                            <div class="col-lg-8 col-md-12 font-weight-bold">
                                : {{ $member->member_investment_experience }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                Pekerjaan
                            </div>
                            <div class="col-lg-8 col-md-12 font-weight-bold">
                                : {{ ($member->job_id) ? $member->job->nama : '' }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                Sumber Pendapatan
                            </div>
                            <div class="col-lg-8 col-md-12 font-weight-bold">
                                : {{ $member->member_income_source }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                Tempat Tanggal Lahir
                            </div>
                            <div class="col-lg-8 col-md-12 font-weight-bold">
                                : {{ $member->member_place_of_birth }}, {{ GeneralHelper::konversiTgl($member->member_date_of_birth, 'ttd') }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                Alamat
                            </div>
                            <div class="col-lg-8 col-md-12 font-weight-bold">
                                : {{ $member->member_address }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                Status Nikah
                            </div>
                            <div class="col-lg-8 col-md-12 font-weight-bold">
                                : {{ ($member->marital_status_id) ? $member->marital_status->nama : '' }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                Nama Pasangan
                            </div>
                            <div class="col-lg-8 col-md-12 font-weight-bold">
                                : {{ $member->member_couple }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                Nama Kandung Ibu
                            </div>
                            <div class="col-lg-8 col-md-12 font-weight-bold">
                                : {{ $member->member_mother }}
                            </div>
                        </div>


                    </div>
                </div>
            </div>

            <div class="col-lg-5">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Berkasi Penunjang
                        </h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                Foto KTP
                            </div>
                            <div class="col-lg-8 col-md-12 font-weight-bold">
                                : <a href="{{ url('member_files/'.$member->member_file_ktp) }}" target="_blank" rel="noopener noreferrer">{{ $member->member_file_ktp }}</a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                Foto Selfie dengan KTP
                            </div>
                            <div class="col-lg-8 col-md-12 font-weight-bold">
                                : <a href="{{ url('member_files/'.$member->member_file_selfie) }}" target="_blank" rel="noopener noreferrer">{{ $member->member_file_selfie }}</a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                Foto NPWP
                            </div>
                            <div class="col-lg-8 col-md-12 font-weight-bold">
                                : <a href="{{ url('member_files/'.$member->member_file_npwp) }}" target="_blank" rel="noopener noreferrer">{{ $member->member_file_npwp }}</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@extends('layouts.member')

@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"> Form Data Pribadi</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Data Pribadi</a></li>
                    <li class="breadcrumb-item active">Form</li>
                </ol>
            </div><!-- /.col -->
        </div>
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Form isi Data Probadi
                        </h5>
                    </div>
                    <div class="card-body">

                        <form action="{{ route('member.profile_update') }}" method="POST" data-remote enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Nama Lengkap <span class="text-red">*</span></label>
                                        <input type="text" name="member_name" required class="form-control" placeholder="Nama lengkap" value="{{ $member->member_name }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Tempat Lahir <span class="text-red">*</span></label>
                                        <input type="text" name="member_place_of_birth" required class="form-control" placeholder="Tempat lahir"
                                            value="{{ $member->member_place_of_birth }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Tanggal Lahir <span class="text-red">*</span></label>
                                        <input type="date" name="member_date_of_birth" required class="form-control"
                                            value="{{ $member->member_date_of_birth }}">
                                    </div>

                                    <div class="form-group">
                                        <label>NIK (Nomor Induk KTP) <span class="text-red">*</span></label>
                                        <input type="text" name="member_nik" required class="form-control" placeholder="NIK" minlength="16" maxlength="16" value="{{ $member->member_nik }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Nomor Telepon <span class="text-red">*</span></label>
                                        <input type="text" name="member_phone" required class="form-control" placeholder="Nomor telepon" value="{{ $member->member_phone }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Nomor NPWP (Opsional)</label>
                                        <input type="text" name="member_npwp" class="form-control" placeholder="Nomor NPWP" value="{{ $member->member_npwp }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Penghasilan Pertahun <span class="text-red">*</span></label>
                                        <input type="text" name="member_income" class="form-control money" required placeholder="Penghasilan pertahun" value="{{ $member->member_income }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Pekerjaan <span class="text-red">*</span></label>
                                        <select name="member_job_id" required class="form-control">
                                            <option value="">-- Pilih Pekerjaan --</option>
                                            @foreach ($jobs as $job)
                                                <option value="{{ $job->id }}" {{ ($job->id == $member->member_job_id) ? 'selected' : '' }}>{{ $job->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Sumber Pendapatan <span class="text-red">*</span></label>
                                        <input type="text" name="member_income_source" required class="form-control" value="{{ $member->member_income_source }}">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Status Nikah <span class="text-red">*</span></label>
                                        <select name="member_marital_status_id" required class="form-control">
                                            <option value="">-- Pilih Status Nikah --</option>
                                            @foreach ($marital_status as $status)
                                                <option value="{{ $status->id }}" {{ ($status->id == $member->member_marital_status_id) ? 'selected' : '' }}>{{ $status->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Nama Pasangan <span class="text-red">*</span></label>
                                        <input type="text" name="member_couple" class="form-control" required placeholder="Nama pasangan" value="{{ $member->member_couple }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Nama Ibu Kandung <span class="text-red">*</span></label>
                                        <input type="text" name="member_mother" required class="form-control" placeholder="Nama ibu kandung" value="{{ $member->member_mother }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Alamat Lengkap <span class="text-red">*</span></label>
                                        <textarea name="member_address" required class="form-control">{{ $member->member_address }}</textarea>
                                    </div>

                                    <hr class="mt-4">

                                    <div class="form-group">
                                        <label>Foto KTP <span class="text-red"></span></label>
                                        <input type="file" name="member_file_ktp" class="form-control form-file">
                                    </div>

                                    <div class="form-group">
                                        <label>Foto Selfie dengan KTP <span class="text-red"></span></label>
                                        <input type="file" name="member_file_selfie" class="form-control form-file">
                                    </div>

                                    <div class="form-group">
                                        <label>Foto NPWP <span class="text-red"></span></label>
                                        <input type="file" name="member_file_npwp" class="form-control form-file">
                                    </div>

                                    <div class="form-group">
                                        <div class="alert alert-warning">
                                            Ketentuan :
                                            <ul>
                                                <li>Format foto : .jpg/.jpeg/.png/.pdf</li>
                                                <li>Ukurang file maksimal 2mb.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <p>(<span class="text-red">*</span>) Waji diisi</p>
                            <button type="submit" class="btn btn-primary btn-sm">
                                SIMPAN DATA PRIBADI
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

<script>
$('.form-file').change(function() {
    var filePath = this.value;

    // Allowing file type
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.pdf)$/i;

    if (!allowedExtensions.exec(filePath)) {
        alert('Jenis file tidak bisa diupload');
        this.value = '';
        return false;
    } else {
        var FileSize = this.files[0].size / 2048 / 2048; // in MB
        if (FileSize > 1) {
            this.value = '';
            alert("Mohon maaf file terlalu besar, maximal file 2 mb.")
            return false;
        }
    }
});

$(document).ready(function(){
    $("input[name='member_nik']").keyup(function(){

    });
});

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        var member_nik = $('input[name="member_nik"]').val();
        var member_nik_choosen = member_nik.substr(8,4);

        var member_date_of_birth = $('input[name="member_date_of_birth"]').val();
        var member_date_of_birth_array = member_date_of_birth.split("-");
        var year = member_date_of_birth_array[0].substr(2,2);
        var month = member_date_of_birth_array[1];
        var month_year = month + '' + year;

        if (month_year != member_nik_choosen) {
            toastr.error("Gagal menyimpan, NIK tidak valid!")
        } else {
            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    toastr.success(res.text)
                    // redirect to how to pay
                    var base_url = {!! json_encode(url('/member/dashboard/')) !!};
                    window.location.href = base_url;
                }
            });
        }
    });
})();
</script>
@endsection

@extends('layouts.member')

@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-12">
                <h1 class="m-0 text-dark"> Verifikasi</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="card shadow-none">
                    <div class="card-body">
                        <h4 class="text-green font-weight-bold">
                            <i class="fas fa-check-circle"></i> &nbsp;&nbsp;
                            Verifikasi alamat email berhasil.
                        </h4>
                    </div>
                </div>
            </div>
        </div>

        @if ($member->member_verified == '1')
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="card shadow-none">
                        <div class="card-body">
                            <h4 class="text-green font-weight-bold">
                                <i class="fas fa-check-circle"></i> &nbsp;&nbsp;
                                Verifikasi identitas berhasil.
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="card shadow-none">
                        <div class="card-body">
                            <h4 class="text-secondary font-weight-bold">
                                <i class="fas fa-spinner"></i> &nbsp;&nbsp;
                                Menunggu verifikasi identitas.
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if ($member->member_actived == '1')
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="card shadow-none">
                        <div class="card-body">
                            <h4 class="text-green font-weight-bold">
                                <i class="fas fa-check-circle"></i> &nbsp;&nbsp;
                                Status anggota sudah aktif.
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="card shadow-none">
                        <div class="card-body">
                            <h4 class="text-secondary font-weight-bold">
                                <i class="fas fa-spinner"></i> &nbsp;&nbsp;
                                Status anggota belum aktif.
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
@endsection

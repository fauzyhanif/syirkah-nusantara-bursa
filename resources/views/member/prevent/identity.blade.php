@extends('layouts.member')

@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-12">
                <h1 class="m-0 text-dark"> Menunggu pengecekan identitas pribadi</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row mb-2">
            <div class="col-lg-12">
                <div class="alert alert-warning shadow-none">
                    <h5 class="font-weight-bold">Mohon maaf, anda belum bisa withdraw</h5>
                    Data identitas pribadi anda sedang dalam proses verifikasi oleh admin, mohon tunggu....
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

@endsection

<table class="table table-striped table-bordered table-sm">
    <thead class="bg-info">
        <th>Nomor Withdraw</th>
        <th width="30%">Tanggal</th>
        <th>Status</th>
        <th>Nilai</th>
    </thead>
    <tbody>
        @foreach ($withdraws as $withdraw)
            <tr>
                <td>
                    <a href="{{ url('member/withdraw/detail', $withdraw->withdraw_id) }}">
                        {{ $withdraw->withdraw_number }}
                    </a>
                </td>
                <td>{{ GeneralHelper::konversiTgl($withdraw->withdraw_date) }}</td>
                <td>{{ $withdraw->withdraw_status->withdraw_status_name }}</td>
                <td>{{ GeneralHelper::rupiah($withdraw->withdraw_nominal) }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

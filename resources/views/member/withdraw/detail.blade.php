@extends('layouts.member')

@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"> Detail Withdraw</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Withdraw</a></li>
                    <li class="breadcrumb-item active">Detail</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                @if ($withdraw->withdraw_status_id == '0')
                    <div class="alert alert-info">
                        <b>Masih menunggu verifikasi dari admin.</b>
                    </div>
                @elseif ($withdraw->withdraw_status_id == '1')
                    <div class="alert alert-success">
                        <b>Withdraw berhasil diverifikasi!</b>
                    </div>
                @elseif ($withdraw->withdraw_status_id == '2')
                    <div class="alert alert-success">
                        <b>Withdraw gagal diverifikasi!</b>
                    </div>
                @endif
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="card shadow-none">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3 text-secondary">Tanggal</div>
                            <div class="col-lg-8">{{ GeneralHelper::konversiTgl($withdraw->withdraw_date) }}</div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 text-secondary">No Withdraw</div>
                            <div class="col-lg-8">{{ $withdraw->withdraw_number }}</div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 text-secondary">Jenis Withdraw</div>
                            <div class="col-lg-8">{{ $withdraw->withdraw_type->saving_type_name }}</div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 text-secondary">Nominal</div>
                            <div class="col-lg-8">{{ GeneralHelper::rupiah($withdraw->withdraw_nominal) }}</div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 text-secondary">Bank</div>
                            <div class="col-lg-8">{{ $withdraw->withdraw_member_rek_number }}</div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    @if ($withdraw->withdraw_status_id == '0')
                        <div class="col-lg-4 mb-sm-2">
                            <a href="{{ url('member/withdraw/form_update', $withdraw->withdraw_id) }}" class="btn btn-sm btn-primary btn-block">UBAH WITHDRAW</a>
                        </div>
                        <div class="col-lg-4 mb-sm-2">
                            <a href="{{ url('member/withdraw/delete', $withdraw->withdraw_id) }}" class="btn btn-sm btn-danger btn-block">HAPUS WITHDRAW</a>
                        </div>
                    @endif
                    <div class="col-lg-4">
                        <a href="{{ route('member.dashboard') }}" class="btn btn-sm btn-outline-primary btn-block">KEMBALI KE DASHBOARD</a>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

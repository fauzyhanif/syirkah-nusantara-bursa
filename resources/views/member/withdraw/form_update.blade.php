@extends('layouts.member')

@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-12">
                <h1 class="m-0 text-dark"> Withdraw</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Form Update Withdraw
                        </h5>
                    </div>
                    <div class="card-body">
                        <form action="{{ url('member/withdraw/update', $withdraw->withdraw_id) }}" method="POST" data-remote>
                            <div class="form-group">
                                <label>Nominal <span class="text-red">*</span></label>
                                <input type="text" name="withdraw_nominal" required class="form-control money" value="{{ $withdraw->withdraw_nominal }}">
                            </div>

                            <div class="form-group">
                                <label>Jenis Tabungan <span class="text-red">*</span></label>
                                <select name="saving_type_id" required class="form-control">
                                    @foreach ($saving_types as $saving_type)
                                        <option value="{{ $saving_type->saving_type_id }}" {{ ($withdraw->saving_type_id == $saving_type->saving_type_id) ? 'selected' : '' }}>{{ $saving_type->saving_type_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Bank <span class="text-red">*</span></label>
                                <select name="withdraw_member_bank" class="form-control">
                                    <option {{ ($withdraw->withdraw_member_bank == 'BRI') ? 'selected' : '' }}>BRI</option>
                                    <option {{ ($withdraw->withdraw_member_bank == 'BNI') ? 'selected' : '' }}>BNI</option>
                                    <option {{ ($withdraw->withdraw_member_bank == 'BCA') ? 'selected' : '' }}>BCA</option>
                                    <option {{ ($withdraw->withdraw_member_bank == 'MANDIRI') ? 'selected' : '' }}>MANDIRI</option>
                                    <option {{ ($withdraw->withdraw_member_bank == 'BSI') ? 'selected' : '' }}>BSI</option>
                                    <option {{ ($withdraw->withdraw_member_bank == 'PERMATA') ? 'selected' : '' }}>PERMATA</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nomor Rekening <span class="text-red">*</span></label>
                                <input type="text" name="withdraw_member_rek_number" class="form-control" value="{{ $withdraw->withdraw_member_rek_number }}">
                            </div>

                            <button type="submit" class="btn btn-primary btn-block btn-sm mt-2">withdraw</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

<script>
$(function() {
    $('.money').mask('000.000.000.000.000', {reverse: true});
});

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text);
                    // redirect to how to pay
                    var base_url = {!! json_encode(url('/member/withdraw/detail/')) !!};
                    var base_url = base_url + "/" + res.withdraw_id;
                    window.location.href = base_url;
                } else {
                    toastr.error(res.text);
                }
            }
        });
    });
})();
</script>
@endsection

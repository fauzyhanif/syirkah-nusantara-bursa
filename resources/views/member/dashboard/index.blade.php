@extends('layouts.member')

@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-12">
                <h1 class="m-0 text-dark"> Dashboard</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-success shadow-none">
                    Selamat datang <b>{{ Auth::user()->name }}</b> di Syirkah Nusantara
                </div>
            </div>
        </div>

        @if ($member->member_nik == '')
        <div class="row mb-2">
            <div class="col-lg-12">
                <div class="alert alert-warning">
                    Data pribadi anda belum lengkap, silahkan lengkapi data anda dengan benar.

                    <br>
                    <a href="{{ route('member.profile_form') }}" class="btn btn-xs btn-primary mt-2">
                        LENGKAPI DATA
                    </a>
                </div>
            </div>
        </div>
        @endif

        @if ($member->member_verified == '0' && $member->member_nik != '')
            <div class="row mb-2">
                <div class="col-lg-12">
                    <div class="alert alert-warning shadow-none">
                        Data identitas pribadi anda sedang dalam proses verifikasi oleh admin, mohon tunggu....
                    </div>
                </div>
            </div>
        @endif

        @if ($member->member_verified == '1' && $member->member_actived == '0')
            <div class="row mb-2">
                <div class="col-lg-12">
                    <div class="alert alert-warning shadow-none">
                        Status keanggotaan belum aktif, silakan lakukan topup untuk mengaktifkan keanggotaan anda.

                        <br>

                        <a href="{{ route('member.topup.form_store') }}" class="btn btn-sm btn-primary font-weight-bold mt-2">
                            TOP UP
                        </a>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-lg-8">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">Saldo</h5>
                    </div>
                    <div class="card-body">
                        <div class="row mt-2" id="view-saving-list">

                        </div>

                        <hr>

                        <div class="row mb-1">
                            <div class="col-lg-6 mb-sm-2">
                                <a href="{{ route('member.topup.form_store') }}" class="btn btn-sm btn-primary btn-block font-weight-bold">
                                    TOP UP
                                </a>
                            </div>

                            <div class="col-lg-6 ">
                                <a href="{{ route('member.withdraw.form_store') }}" class="btn btn-sm btn-primary btn-block font-weight-bold">
                                    WITHDRAW
                                </a>
                            </div>
                       </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">Portfolio</h5>
                    </div>
                    <div class="card-body">
                        <h6 class="card-title">Total Asset : Rp.-</h6>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Topup
                        </h5>
                    </div>
                    <div class="card-body" style="overflow-x: scroll" id="view-table-topup">

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Withdraw
                        </h5>
                    </div>
                    <div class="card-body" style="overflow-x: scroll" id="view-table-withdraw">

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Transaksi Saham
                        </h5>
                    </div>
                    <div class="card-body" style="overflow-x: scroll">
                        <table class="table table-striped table-bordered table-sm">
                            <thead class="bg-info">
                                <th>Tanggal</th>
                                <th>Kode Aset</th>
                                <th>Lembar Saham</th>
                                <th>Nilai</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="4" class="text-center">Coming Soon ..</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">
                            Dividen
                        </h5>
                    </div>
                    <div class="card-body" style="overflow-x: scroll">
                        <table class="table table-striped table-bordered table-sm">
                            <thead class="bg-info">
                                <th>Tanggal</th>
                                <th>Kode Aset</th>
                                <th>Lembar Saham</th>
                                <th>Net Dividen</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="4" class="text-center">Coming Soon ..</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

<script>

$(function() {
    call_view_saving_list();
    call_view_table_topup();
    call_view_table_withdraw();
})

function call_view_saving_list() {
    $.ajax({
        type: 'GET',
        url: {!! json_encode(url('/member/dashboard/saving_list/')) !!},
        dataType:'html',
        success: function(res){
           $('#view-saving-list').html(res)
        }
    });
}

function call_view_table_topup() {
    $.ajax({
        type: 'GET',
        url: {!! json_encode(url('/member/topup/list/')) !!},
        dataType:'html',
        success: function(res){
           $('#view-table-topup').html(res)
        }
    });
}

function call_view_table_withdraw() {
    $.ajax({
        type: 'GET',
        url: {!! json_encode(url('/member/withdraw/list/')) !!},
        dataType:'html',
        success: function(res){
           $('#view-table-withdraw').html(res)
        }
    });
}
</script>
@endsection

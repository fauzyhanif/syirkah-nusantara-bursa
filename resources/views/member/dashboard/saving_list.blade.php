@foreach ($savings as $saving)
    <div class="col-lg-4 mb-sm-2">
        <div class="my-2 border border-secondary rounded">
            <div class="m-2">
                <span class="text-secondary">{{ $saving->saving_types->saving_type_name }}</span>
                <h4 class="font-weight-bold">
                    Rp {{ GeneralHelper::rupiah($saving->amount - $saving->amount_withdrawn) }}
                </h4>
            </div>
        </div>
    </div>
@endforeach

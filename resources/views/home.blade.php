@extends('layouts.member')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-12">
                <h1 class="m-0 text-dark"> Verifikasi Email Berhasil</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h5>
                        {{ __('Verifikasi email berhasil!') }}
                    </h5>

                    <br>

                    <h5>Selanjutnya, silahkan lengkapi data pribadi anda dengan klik tombol dibawah ini</h5>
                    <a href="{{ route('member.profile_form') }}" class="btn btn-primary btn-sm">ISI DATA PRIBADI</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

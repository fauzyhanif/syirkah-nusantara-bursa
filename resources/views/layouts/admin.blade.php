<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Bursa Syirkah | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/plugins/fontawesome-free/css/all.min.css') }}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Tempusdominus Bbootstrap 4 -->
        <link rel="stylesheet"
            href="{{ url('public/admin-lte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
        <!-- Daterangepicker -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/plugins/daterangepicker/daterangepicker.css') }}">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
        {{-- datatable --}}
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css" />
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">
        <!-- overlayScrollbars -->
        <link rel="stylesheet"
            href="{{  url('public/admin-lte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <!-- Daterangepicker -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/plugins/daterangepicker/daterangepicker.css') }}">
        <!-- fullCalendar -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/plugins/fullcalendar/main.min.css') }}">
        <link rel="stylesheet" href="{{  url('public/admin-lte/plugins/fullcalendar-daygrid/main.min.css') }}">
        <link rel="stylesheet" href="{{  url('public/admin-lte/plugins/fullcalendar-timegrid/main.min.css') }}">
        <link rel="stylesheet" href="{{  url('public/admin-lte/plugins/fullcalendar-bootstrap/main.min.css') }}">
        {{-- Toastr --}}
        <link rel="stylesheet" href="{{ url('public/admin-lte/plugins/toastr/toastr.css') }}">
        {{-- Select2 --}}
        <link rel="stylesheet" href="{{ url('public/admin-lte/plugins/select2/css/select2.min.css') }}">
        <link rel="stylesheet"
            href="{{ url('public/admin-lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link
            href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700&display=swap"
            rel="stylesheet">

        <!-- jQuery -->
        <script src="{{ url('public/admin-lte/plugins/jquery/jquery.min.js') }}"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <!-- Bootstrap 4 -->
        <script src="{{ url('public/admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{ url('public/admin-lte/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('public/admin-lte/dist/js/adminlte.min.js') }}"></script>
        <!-- Jquery Redirect -->
        <script src="{{ url('public/admin-lte/plugins/jquery-redirect/jquery-redirect.js') }}"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="{{ url('public/admin-lte/dist/js/demo.js') }}"></script>
        <script src="{{ url('public/admin-lte/plugins/moment/moment.min.js') }}"></script>
        <script src="{{ url('public/admin-lte/plugins/fullcalendar/main.min.js') }}"></script>
        <script src="{{ url('public/admin-lte/plugins/fullcalendar-daygrid/main.min.js') }}"></script>
        <script src="{{ url('public/admin-lte/plugins/fullcalendar-timegrid/main.min.js') }}"></script>
        <script src="{{ url('public/admin-lte/plugins/fullcalendar-interaction/main.min.js') }}"></script>
        <script src="{{ url('public/admin-lte/plugins/fullcalendar-bootstrap/main.min.js') }}"></script>
        <!-- daterangepicker -->
        <script src="{{ url('public/admin-lte/plugins/moment/moment.min.js') }}"></script>
        <script src="{{ url('public/admin-lte/plugins/daterangepicker/daterangepicker.js') }}"></script>
        <!-- Tempusdominus Bootstrap 4 -->
        <script
            src="{{ url('public/admin-lte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}">
        </script>
        {{-- Toastr --}}
        <script src="{{ url('public/admin-lte/plugins/toastr/toastr.min.js') }}"></script>
        {{-- Datatable --}}
        <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
        <!-- overlayScrollbars -->
        <script src="{{ url('public/admin-lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}">
        </script>
        <!-- bs-custom-file-input -->
        <script src="{{ url('public/admin-lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
        <!-- select2 -->
        <script src="{{ url('public/admin-lte/plugins/select2/js/select2.min.js') }}"></script>
        <!-- jquery redirect -->
        <script src="{{ url('public/js/jquery-redirect.js') }}"></script>
        <!-- jquery mask -->
        <script src="{{ url('public/admin-lte/plugins/jquery-mask/dist/jquery.mask.min.js') }}"></script>

        <style>
            @media (max-width: 576px) {
                .mb-sm-1 {
                    margin-bottom: 4px !important;
                }

                .mb-sm-2 {
                    margin-bottom: 6px !important;
                }
            }
        </style>

        <script>
            $.widget.bridge('uibutton', $.ui.button)

            $(function() {
                $('.money').mask('000.000.000.000.000', {reverse: true});
            });

            $(function() {
                call_data_notification();
            });

            function call_data_notification() {
                $.ajax({
                    type: 'GET',
                    url: {!! json_encode(url('/notification_get_data')) !!},
                    dataType: 'json',
                    success: function(res){
                        $('.notification-new').text(res.notification_new)
                        $('.notification-topup').text(res.notification_topup)
                        $('.notification-withdraw').text(res.notification_withdraw)
                    }
                });
            }

            function read_notification() {
                $.ajax({
                    type: 'GET',
                    url: {!! json_encode(url('/notification_update_read')) !!},
                    dataType: 'json',
                    success: function(res){
                    }
                });
            }
        </script>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            <nav class="main-header navbar navbar-expand border-bottom-0 navbar-light">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                    </li>
                </ul>

                <!-- Right navbar links -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link" data-toggle="dropdown" href="javascript:void(0)" onclick="read_notification()">
                            <i class="far fa-bell"></i>
                            <span class="badge badge-danger navbar-badge notification-new">0</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                            <span class="dropdown-item dropdown-header"><span class="notification-new">0</span> Notifikasi Baru</span>
                            <div class="dropdown-divider"></div>
                            <a href="{{ route('admin.topup') }}" class="dropdown-item">
                                <i class="fas fa-file-import mr-2"></i> <span class="notification-topup">0</span> Topup baru
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="{{ route('admin.withdraw') }}" class="dropdown-item">
                                <i class="fas fa-file-export mr-2"></i> <span class="notification-withdraw">0</span> Withdraw baru
                            </a>
                        </div>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link">
                            <i class="fas fa-sign-out-alt"></i> &nbsp;
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" data-toggle="dropdown" href="#">
                            <i class="far fa-user"></i> &nbsp;
                            {{ Auth::user()->name }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                            <a href="{{ route('logout') }}" class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fas fa-sign-out-alt mr-2"></i> Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </nav>

            <aside class="main-sidebar elevation-4 sidebar-dark-orange">
                <!-- Brand Logo -->
                <a href="index3.html" class="brand-link">
                    <img src="" alt=""class="brand-image img-circle elevation-3">
                    <span class="brand-text font-weight-light"><b>Syikrah Nusantara</b> Bursa</span>
                </a>

                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column nav-legacy nav-flat" data-widget="treeview" role="menu"
                            data-accordion="false">
                            <li class="nav-item">
                                <a href="{{ url('/admin/dashboard') }}" class="nav-link {{ (request()->is('/admin/dashboard')) ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>Dashboard</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('/admin/member') }}" class="nav-link {{ (request()->is('/admin/member')) ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>Data Member</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('/admin/topup') }}" class="nav-link {{ (request()->is('/admin/topup')) ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-file-import"></i>
                                    <p>Topup Member</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('/admin/withdraw') }}" class="nav-link {{ (request()->is('/admin/withdraw')) ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-file-export"></i>
                                    <p>Withdraw Member</p>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </aside>

            <div class="content-wrapper">
                <div class="container-fluid">
                    @yield('content')
                </div>
            </div>

            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <strong>Copyright  Bursa Syirkah &copy; 2022</strong>
                <div class="float-right d-none d-sm-inline-block">
                    <b>Version</b> 1.0-beta
                </div>
            </footer>
        </div>
    </body>

</html>

<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Bursa Syirkah | Daftar</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ url('public/admin-lte/plugins/fontawesome-free/css/all.min.css') }}">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{ url('public/admin-lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ url('public/admin-lte/dist/css/adminlte.min.css') }}">
        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        {{-- Toastr --}}
        <link rel="stylesheet" href="{{ url('public/admin-lte/plugins/toastr/toastr.css') }}">

        <!-- jQuery -->
        <script src="{{ url('public/admin-lte/plugins/jquery/jquery.min.js') }}"></script>
        {{-- bootstrap --}}
        <script src="{{ url('public/admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <!-- Scripts -->
        <script src="{{ url('js/app.js') }}" defer></script>
        {{-- Toastr --}}
        <script src="{{ url('public/admin-lte/plugins/toastr/toastr.min.js') }}"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('public/admin-lte/dist/js/adminlte.min.js') }}"></script>
        <!-- jquery mask -->
        <script src="{{ url('public/admin-lte/plugins/jquery-mask/dist/jquery.mask.min.js') }}"></script>

        <style>
            @media (max-width: 576px) {
                .mb-sm-1 {
                    margin-bottom: 4px !important;
                }

                .mb-sm-2 {
                    margin-bottom: 6px !important;
                }
            }
        </style>
        <script>
            $(function() {
                $('.money').mask('000.000.000.000.000', {reverse: true});
            });
        </script>

    </head>

    <body class="hold-transition layout-top-nav">
        <div class="wrapper">

            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
                <div class="container">
                    <a href="{{ route('member.dashboard') }}" class="navbar-brand font-weight-bold">
                        <img src="{{ url('public/company_file/logo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3">
                        <span class="brand-text font-weight-light">Bursa Syirkah</span>
                    </a>
                    <div class="collapse navbar-collapse order-3" id="navbarCollapse">
                        <!-- Left navbar links -->
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a href="{{ route('member.verifikasi') }}" class="nav-link">Verifikasi</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('member.topup.form_store') }}" class="nav-link">Topup</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('member.withdraw.form_store') }}" class="nav-link">Withdraw</a>
                            </li>

                        </ul>
                    </div>

                    <!-- Right navbar links -->
                    <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link" data-toggle="dropdown" href="#">
                                <i class="far fa-user"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">

                                <a href="{{ route('member.profile') }}" class="dropdown-item">
                                    <i class="fas fa-user"></i> &nbsp;
                                    {{ Auth::user()->name }}
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-red" href="{{ route('logout') }}"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        <li class="nav-item">
                            <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- /.navbar -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                @yield('content')

            </div>
            <!-- /.content-wrapper -->

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
                <div class="p-3">
                    <h5>Title</h5>
                    <p>Sidebar content</p>
                </div>
            </aside>
            <!-- /.control-sidebar -->

            <!-- Main Footer -->
            <footer class="main-footer">
                <!-- To the right -->
                <div class="float-right d-none d-sm-inline">
                    Anything you want
                </div>
                <!-- Default to the left -->
                <strong>Copyright Bursa Syirkah &copy; 2022</strong>
            </footer>
        </div>
        <!-- ./wrapper -->

        <!-- REQUIRED SCRIPTS -->


    </body>

</html>

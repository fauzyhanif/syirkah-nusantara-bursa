@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Data Member</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col-lg-2 mb-sm-2">
                <select name="member_verified" class="form-control form-control-sm">
                    <option value="*">Semua status verifikasi</option>
                    <option value="1">Berhasil verifikasi</option>
                    <option value="2">Gagal verifikasi</option>
                </select>
            </div>

            <div class="col-lg-2 mb-sm-2">
                <select name="member_actived" class="form-control form-control-sm">
                    <option value="*">Semua status aktif</option>
                    <option value="0">Belum aktif</option>
                    <option value="1">Sudah aktif</option>
                    <option value="2">Non aktif</option>
                </select>
            </div>

            <div class="col-lg-3 mb-sm-2">
                <input type="text" name="member_name" class="form-control form-control-sm" placeholder="Nama Member">
            </div>

            <div class="col-lg-1">
                <button type="button" class="btn btn-sm btn-primary" onclick="call_view_list()">
                    TAMPILKAN
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-none">
                    <div class="card-body" id="view-list" style="overflow-x: scroll">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(function() {
    call_view_list(1);
})

function call_view_list(current_page) {
    var member_verified = $('select[name="member_verified"]').val();
    var member_actived = $('select[name="member_actived"]').val();
    var member_name = $('input[name="member_name"]').val();
    var _token = '<?php echo csrf_token() ?>';
    var data = "member_verified="+member_verified+"&member_actived="+member_actived+"&member_name="+member_name+"&_token="+_token;

    $.ajax({
        type: 'POST',
        url: {!! json_encode(url('/admin/member/list?page=')) !!}+current_page,
        dataType:'html',
        data:data,
        success: function(res){
           $('#view-list').html(res)
        }
    });
}
</script>
@endsection

<table class="table table-striped table-bordered table-sm">
    <thead class="bg-info">
        <th>Nomor Withdraw</th>
        <th width="30%">Tanggal</th>
        <th>Jenis</th>
        <th>Status</th>
        <th>Nilai</th>
    </thead>
    <tbody>
        @foreach ($withdraws as $withdraw)
            <tr>
                <td>{{ $withdraw->withdraw_number }}</td>
                <td>{{ GeneralHelper::konversiTgl($withdraw->withdraw_date) }}</td>
                <td>{{ $withdraw->withdraw_type->saving_type_name }}</td>
                <td>{{ $withdraw->withdraw_status->withdraw_status_name }}</td>
                <td>{{ GeneralHelper::rupiah($withdraw->withdraw_nominal) }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

@if ($withdraws->lastPage() > 1)
<ul class="pagination">
    <li class="page-item {{ ($withdraws->currentPage() == 1) ? ' disabled' : '' }}">
        <a class="page-link" href="javascript:void(0)" onclick="call_view_list_withdraw(1)">Previous</a>
    </li>
    @for ($i = 1; $i <= $withdraws->lastPage(); $i++)
        <li class="page-item {{ ($withdraws->currentPage() == $i) ? ' active' : '' }}">
            <a class="page-link" href="javascript:void(0)" onclick="call_view_list_withdraw({{ $i }})">{{ $i }}</a>
        </li>
        @endfor
        <li class="page-item {{ ($withdraws->currentPage() == $withdraws->lastPage()) ? ' disabled' : '' }}">
            <a class="page-link" href="javascript:void(0)"
                onclick="call_view_list_withdraw({{ $withdraws->url($withdraws->currentPage()+1) }})">Next</a>
        </li>
</ul>
@endif

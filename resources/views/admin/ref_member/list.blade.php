<table class="table table-sm table-bordered table-hover mb-3">
    <thead class="bg-info">
        <th class="text-center" width="5%">No</th>
        <th>Nama</th>
        <th>Email</th>
        <th>No Telpon</th>
        <th>Alamat</th>
        <th width="12%">Verifikasi</th>
        <th width="12%">Aktif</th>
        <th width="12%">Aksi</th>
    </thead>
    <tbody>

        <?php
            if($datas->currentPage() > 1) {
                $current = $datas->currentPage() - 1;
                $no = (15 * $current) + 1;
            } else {
                $no = 1;
            }
        ?>
        @foreach ($datas as $data)
            <tr>
                <td class="text-center">{{ $no }}.</td>
                <td>{{ $data->member_name }}</td>
                <td>{{ $data->email }}</td>
                <td>{{ $data->member_phone }}</td>
                <td>{{ $data->member_address }}</td>
                <td>
                    @if ($data->member_verified == '0')
                        <span class="text-secondary">
                            Menuggu verifikasi
                        </span>
                    @elseif ($data->member_verified == '1')
                        <span class="text-green">
                            Berhasil
                        </span>
                    @else
                        <span class="text-red">
                            Gagal
                        </span>
                    @endif
                </td>
                <td>
                    @if ($data->member_actived == '0')
                        <span class="text-secondary">
                            Belum aktif
                        </span>
                    @elseif ($data->member_actived == '1')
                        <span class="text-green">
                            Aktif
                        </span>
                    @else
                        <span class="text-red">
                            Non Aktif
                        </span>
                    @endif
                </td>
                <td>
                    <a href="{{ url('admin/member/detail', $data->member_id) }}" class="btn btn-xs btn-primary">
                        <i class="fas fa-eye"></i> Detail
                    </a>
                </td>
            </tr>
        @php
            $no = $no + 1;
        @endphp
        @endforeach
    </tbody>
</table>

@if ($datas->lastPage() > 1)
<ul class="pagination">
    <li class="page-item {{ ($datas->currentPage() == 1) ? ' disabled' : '' }}">
        <a class="page-link" href="javascript:void(0)" onclick="call_view_list(1)">Previous</a>
    </li>
    @for ($i = 1; $i <= $datas->lastPage(); $i++)
        <li class="page-item {{ ($datas->currentPage() == $i) ? ' active' : '' }}">
            <a class="page-link" href="javascript:void(0)" onclick="call_view_list({{ $i }})">{{ $i }}</a>
        </li>
        @endfor
        <li class="page-item {{ ($datas->currentPage() == $datas->lastPage()) ? ' disabled' : '' }}">
            <a class="page-link" href="javascript:void(0)" onclick="call_view_list({{ $datas->url($datas->currentPage()+1) }})">Next</a>
        </li>
</ul>
@endif

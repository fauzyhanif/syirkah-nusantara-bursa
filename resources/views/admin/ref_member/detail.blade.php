@extends('layouts.admin')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('admin.member') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Member
            </a>
        </li>
        <li class="breadcrumb-item active">Detail Member</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card shadow-none">
                            <div class="card-header">
                                <h5 class="card-title m-0">
                                    Data Pribadi
                                </h5>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12 text-secondary">
                                        Nama Lengkap
                                    </div>
                                    <div class="col-lg-8 col-md-12 font-weight-bold">
                                        {{ $member->member_name }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-12 text-secondary">
                                        Email
                                    </div>
                                    <div class="col-lg-8 col-md-12 font-weight-bold">
                                        {{ $member->user->email }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-md-12 text-secondary">
                                        Nomor Telepon
                                    </div>
                                    <div class="col-lg-8 col-md-12 font-weight-bold">
                                        {{ $member->member_phone }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-md-12 text-secondary">
                                        NIK (Nomor Induk KTP)
                                    </div>
                                    <div class="col-lg-8 col-md-12 font-weight-bold">
                                        {{ $member->member_nik }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-md-12 text-secondary">
                                        Nomor NPWP
                                    </div>
                                    <div class="col-lg-8 col-md-12 font-weight-bold">
                                        {{ $member->member_npwp }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-md-12 text-secondary">
                                        Penghasilan Pertahun
                                    </div>
                                    <div class="col-lg-8 col-md-12 font-weight-bold">
                                        {{ GeneralHelper::rupiah($member->member_income) }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-md-12 text-secondary">
                                        Pengalaman Investasi
                                    </div>
                                    <div class="col-lg-8 col-md-12 font-weight-bold">
                                        {{ $member->member_investment_experience }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-md-12 text-secondary">
                                        Pekerjaan
                                    </div>
                                    <div class="col-lg-8 col-md-12 font-weight-bold">
                                        {{ ($member->job_id) ? $member->job->nama : '' }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-md-12 text-secondary">
                                        Sumber Pendapatan
                                    </div>
                                    <div class="col-lg-8 col-md-12 font-weight-bold">
                                        {{ $member->member_income_source }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-md-12 text-secondary">
                                        Tempat Tanggal Lahir
                                    </div>
                                    <div class="col-lg-8 col-md-12 font-weight-bold">
                                        {{ $member->member_place_of_birth }}, {{ GeneralHelper::konversiTgl($member->member_date_of_birth) }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-md-12 text-secondary">
                                        Alamat
                                    </div>
                                    <div class="col-lg-8 col-md-12 font-weight-bold">
                                        {{ $member->member_address }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-md-12 text-secondary">
                                        Status Nikah
                                    </div>
                                    <div class="col-lg-8 col-md-12 font-weight-bold">
                                        {{ ($member->marital_status_id) ? $member->marital_status->nama : '' }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-md-12 text-secondary">
                                        Nama Pasangan
                                    </div>
                                    <div class="col-lg-8 col-md-12 font-weight-bold">
                                        {{ $member->member_couple }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-md-12 text-secondary">
                                        Nama Kandung Ibu
                                    </div>
                                    <div class="col-lg-8 col-md-12 font-weight-bold">
                                        {{ $member->member_mother }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card shadow-none">
                            <div class="card-header">
                                <h5 class="card-title m-0">
                                    Berkasi Penunjang
                                </h5>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12 text-secondary">
                                        Foto KTP
                                    </div>
                                    <div class="col-lg-8 col-md-12 font-weight-bold">
                                        <a href="{{ url('member_files/'.$member->member_file_ktp) }}" target="_blank"
                                            rel="noopener noreferrer">{{ $member->member_file_ktp }}</a>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-md-12 text-secondary">
                                        Foto Selfie dengan KTP
                                    </div>
                                    <div class="col-lg-8 col-md-12 font-weight-bold">
                                        <a href="{{ url('member_files/'.$member->member_file_selfie) }}" target="_blank"
                                            rel="noopener noreferrer">{{ $member->member_file_selfie }}</a>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-md-12 text-secondary">
                                        Foto NPWP
                                    </div>
                                    <div class="col-lg-8 col-md-12 font-weight-bold">
                                        <a href="{{ url('member_files/'.$member->member_file_npwp) }}" target="_blank"
                                            rel="noopener noreferrer">{{ $member->member_file_npwp }}</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card shadow-none">
                            <div class="card-header">
                                <h5 class="card-title m-0">
                                    Saldo
                                </h5>
                            </div>
                            <div class="card-body">
                                <div class="row" id="view-saving-list"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card shadow-none">
                            <div class="card-header">
                                <h5 class="card-title m-0">
                                    Daftar Topup
                                </h5>
                            </div>
                            <div class="card-body">
                                <div class="row" id="view-list-topup"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card shadow-none">
                            <div class="card-header">
                                <h5 class="card-title m-0">
                                    Daftar Withdraw
                                </h5>
                            </div>
                            <div class="card-body">
                                <div class="row" id="view-list-withdraw"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">Form Verifikasi Data Member</h5>
                    </div>
                    <div class="card-body">
                        <form name="verification_pii" action="{{ url('admin/member/update_status', $member->member_id) }}" method="POST">
                            <div class="form-group">
                                <label>Status verifikasi data identitas</label>
                                <select name="member_verified" id="" class="form-control">
                                    <option value="0" {{ ($member->member_verified == '0') ? 'selected' : '' }}>Belum Diverifikasi</option>
                                    <option value="1" {{ ($member->member_verified == '1') ? 'selected' : '' }}>Berhasil Diverifikasi</option>
                                    <option value="2" {{ ($member->member_verified == '2') ? 'selected' : '' }}>Gagal Diverifikasi</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Status aktif anggota</label>
                                <select name="member_actived" id="" class="form-control">
                                    <option value="0" {{ ($member->member_actived == '0') ? 'selected' : '' }}>Belum Aktif</option>
                                    <option value="1" {{ ($member->member_actived == '1') ? 'selected' : '' }}>Sudah Aktif</option>
                                    <option value="2" {{ ($member->member_actived == '2') ? 'selected' : '' }}>Non Aktif</option>
                                </select>
                            </div>

                            <hr>

                            <button type="submit" class="btn btn-sm btn-block btn-success">
                                SIMPAN STATUS
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('admin.trnsct_topup.form')
@include('admin.trnsct_withdraw.form')

<script>
$(function() {
    call_view_saving_list();
    call_view_list_topup(1);
    call_view_list_withdraw(1);
});

function call_view_saving_list() {
    $.ajax({
        type: 'GET',
        url: {!! json_encode(url('/admin/member/saving_list', $member->member_id)) !!},
        dataType:'html',
        success: function(res){
           $('#view-saving-list').html(res)
        }
    });
}

function call_view_list_topup(current_page) {
    $.ajax({
        type: 'GET',
        url: {!! json_encode(url('/admin/member/list_topup', $member->member_id)) !!} + '?page=' + current_page,
        dataType:'html',
        success: function(res){
           $('#view-list-topup').html(res)
        }
    });
}

function call_view_list_withdraw(current_page) {
    $.ajax({
        type: 'GET',
        url: {!! json_encode(url('/admin/member/list_withdraw', $member->member_id)) !!} + '?page=' + current_page,
        dataType:'html',
        success: function(res){
           $('#view-list-withdraw').html(res)
        }
    });
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[name="verification_pii"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                toastr.success(res.text)
            }
        });
    });
})();
</script>
@endsection

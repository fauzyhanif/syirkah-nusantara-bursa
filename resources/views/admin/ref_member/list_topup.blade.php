<table class="table table-striped table-bordered table-sm">
    <thead class="bg-info">
        <th>Nomor Topup</th>
        <th width="30%">Tanggal</th>
        <th>Jenis</th>
        <th>Status</th>
        <th>Nilai</th>
    </thead>
    <tbody>
        @foreach ($topups as $topup)
            <tr>
                <td>{{ $topup->topup_number }}</td>
                <td>{{ GeneralHelper::konversiTgl($topup->topup_date) }}</td>
                <td>{{ $topup->topup_type->saving_type_name }}</td>
                <td>{{ $topup->topup_status->topup_status_name }}</td>
                <td>{{ GeneralHelper::rupiah($topup->topup_nominal) }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

@if ($topups->lastPage() > 1)
<ul class="pagination">
    <li class="page-item {{ ($topups->currentPage() == 1) ? ' disabled' : '' }}">
        <a class="page-link" href="javascript:void(0)" onclick="call_view_list_topup(1)">Previous</a>
    </li>
    @for ($i = 1; $i <= $topups->lastPage(); $i++)
        <li class="page-item {{ ($topups->currentPage() == $i) ? ' active' : '' }}">
            <a class="page-link" href="javascript:void(0)" onclick="call_view_list_topup({{ $i }})">{{ $i }}</a>
        </li>
    @endfor
    <li class="page-item {{ ($topups->currentPage() == $topups->lastPage()) ? ' disabled' : '' }}">
        <a class="page-link" href="javascript:void(0)"
            onclick="call_view_list_topup({{ $topups->url($topups->currentPage()+1) }})">Next</a>
    </li>
</ul>
@endif

<table class="table table-sm table-bordered table-hover mb-3">
    <thead class="bg-info">
        <th class="text-center" width="5%">No</th>
        <th width="15%">Tanggal / Jam</th>
        <th width="15%">ID Transaksi</th>
        <th>Nama</th>
        <th width="15%">Jenis Topup</th>
        <th width="12%">Jumlah</th>
        <th width="12%">Status</th>
        <th width="10%">Verifikasi</th>
    </thead>
    <tbody>
        <?php
            if($datas->currentPage() > 1) {
                $current = $datas->currentPage() - 1;
                $no = (15 * $current) + 1;
            } else {
                $no = 1;
            }
        ?>
        @foreach ($datas as $item)
        <tr>
            <td class="text-center">{{ $no }}.</td>
            <td>{{ GeneralHelper::konversiTgl($item->topup_date) }}</td>
            <td>{{ $item->topup_number }}</td>
            <td>{{ $item->member_name }}</td>
            <td>{{ $item->saving_type_name }}</td>
            <td>{{ GeneralHelper::rupiah($item->topup_nominal) }}</td>
            <td>
                @if ($item->topup_status_id == '0' && date('Y-m-d H:i') < $item->topup_expired)
                    <span class="text-secondary">
                        Belum Transfer
                    </span>
                @elseif ($item->topup_status_id == '0' && date('Y-m-d H:i') > $item->topup_expired)
                    <span class="text-red">
                        Expired
                    </span>
                @elseif ($item->topup_status_id == '1')
                    <span class="text-green">
                        Berhasil
                    </span>
                @elseif ($item->topup_status_id == '2')
                    <span class="text-red">
                        Gagal
                    </span>
                @elseif ($item->topup_status_id == '3')
                    <span class="text-info">
                        Sudah Transfer
                    </span>
                @endif
            </td>
            <td>
                <button type="button" class="btn btn-xs btn-success" onclick="show_form_verification('{{ $item->topup_id }}')">
                    <i class="fas fa-check-circle"></i> Verifikasi
                </button>
            </td>
        </tr>
        @php $no = $no + 1 @endphp
        @endforeach
    </tbody>
</table>

@if ($datas->lastPage() > 1)
<ul class="pagination">
    <li class="page-item {{ ($datas->currentPage() == 1) ? ' disabled' : '' }}">
        <a class="page-link" href="javascript:void(0)" onclick="call_view_list(1)">Previous</a>
    </li>
    @for ($i = 1; $i <= $datas->lastPage(); $i++)
        <li class="page-item {{ ($datas->currentPage() == $i) ? ' active' : '' }}">
            <a class="page-link" href="javascript:void(0)" onclick="call_view_list({{ $i }})">{{ $i }}</a>
        </li>
        @endfor
        <li class="page-item {{ ($datas->currentPage() == $datas->lastPage()) ? ' disabled' : '' }}">
            <a class="page-link" href="javascript:void(0)"
                onclick="call_view_list({{ $datas->url($datas->currentPage()+1) }})">Next</a>
        </li>
</ul>
@endif

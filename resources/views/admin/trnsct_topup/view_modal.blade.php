<!-- Modal Header -->
<div class="modal-header">
    <h4 class="modal-title">Form verifikasi topup member</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<!-- Modal body -->
<form name="topup" action="{{ url('admin/topup/verification', $topup->topup_id) }}" method="POST">
    @csrf
    <input type="hidden" name="topup_member_id" value="{{ $topup->topup_member_id }}">
    <input type="hidden" name="topup_nominal" value="{{ $topup->topup_nominal }}">
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-4 col-md-12">
                Nama Member
            </div>
            <div class="col-lg-8 col-md-12 font-weight-bold">
                : {{ $topup->topup_member->member_name }}
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-12">
                Tanggal Top Up
            </div>
            <div class="col-lg-8 col-md-12 font-weight-bold">
                : {{ GeneralHelper::konversiTgl($topup->topup_date) }}
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-12">
                ID Transaksi
            </div>
            <div class="col-lg-8 col-md-12 font-weight-bold">
                : {{ $topup->topup_number }}
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-12">
                Nominal Top Up
            </div>
            <div class="col-lg-8 col-md-12 font-weight-bold">
                : Rp {{ GeneralHelper::rupiah($topup->topup_nominal) }}
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-12">
                Jenis Top Up
            </div>
            <div class="col-lg-8 col-md-12 font-weight-bold">
                : {{ $topup->topup_type->saving_type_name }}
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-12">
                Bank
            </div>
            <div class="col-lg-8 col-md-12 font-weight-bold">
                : {{ $topup->topup_member_bank }}
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-12">
                Nomor Rekening
            </div>
            <div class="col-lg-8 col-md-12 font-weight-bold">
                : {{ $topup->topup_member_rek_number }}
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-12">
                Status
            </div>
            <div class="col-lg-8 col-md-12 font-weight-bold">
                : {{ $topup->topup_status->topup_status_name }}
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-lg-4">
                <div class="form-group">
                    <label>Tanggal verifikasi</label>
                    <input type="date" name="topup_verification_date" class="form-control" value="{{ ($topup->topup_verification_date) ? $topup->topup_verification_date : date('Y-m-d') }}">
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label>Status</label>
                    <select name="topup_status_id" class="form-control">
                        <option value="">-- Pilih Status --</option>
                        @foreach ($topup_status as $status)
                            <option value="{{ $status->topup_status_id }}" {{ ($topup->topup_status_id == $status->topup_status_id) ? 'selected' : '' }}>{{ $status->topup_status_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label>Masuk ke bank perusahaan</label>
                    <select name="topup_company_bank_id" id="" class="form-control">
                        <option value="">-- Pilih bank perusahaan --</option>
                        <option value="1" {{ ($topup->topup_company_bank_id == '1') ? 'selected' : '' }}>BRI  742701002047538 An Taufik Hidayat</option>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success btn-sm">SIMPAN VERIFIKASI</button>
    </div>
</form>

<script>
(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[name="topup"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');
        var topup_status_id = form.find('select[name="topup_status_id"]').val();
        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                toastr.success(res.text);
                call_view_list(1);
            }
        });
        $('#modal-topup').modal('hide');
    });
})();
</script>

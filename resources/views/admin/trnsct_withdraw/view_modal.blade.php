<!-- Modal Header -->
<div class="modal-header">
    <h4 class="modal-title">Form verifikasi withdraw member</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<!-- Modal body -->
<form name="withdraw" action="{{ url('admin/withdraw/verification', $withdraw->withdraw_id) }}" method="POST">
    @csrf
    <input type="hidden" name="withdraw_member_id" value="{{ $withdraw->withdraw_member_id }}">
    <input type="hidden" name="withdraw_nominal" value="{{ $withdraw->withdraw_nominal }}">
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-4 col-md-12">
                Nama Member
            </div>
            <div class="col-lg-8 col-md-12 font-weight-bold">
                : {{ $withdraw->withdraw_member->member_name }}
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-12">
                Tanggal Withdraw
            </div>
            <div class="col-lg-8 col-md-12 font-weight-bold">
                : {{ GeneralHelper::konversiTgl($withdraw->withdraw_date) }}
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-12">
                ID Transaksi
            </div>
            <div class="col-lg-8 col-md-12 font-weight-bold">
                : {{ $withdraw->withdraw_number }}
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-12">
                Nominal Withdraw
            </div>
            <div class="col-lg-8 col-md-12 font-weight-bold">
                : Rp {{ GeneralHelper::rupiah($withdraw->withdraw_nominal) }}
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-12">
                Jenis Withdraw
            </div>
            <div class="col-lg-8 col-md-12 font-weight-bold">
                : {{ $withdraw->withdraw_type->saving_type_name }}
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-12">
                Bank
            </div>
            <div class="col-lg-8 col-md-12 font-weight-bold">
                : {{ $withdraw->withdraw_member_bank }}
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-12">
                Nomor Rekening
            </div>
            <div class="col-lg-8 col-md-12 font-weight-bold">
                : {{ $withdraw->withdraw_member_rek_number }}
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-12">
                Status
            </div>
            <div class="col-lg-8 col-md-12 font-weight-bold">
                : {{ $withdraw->withdraw_status->withdraw_status_name }}
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-lg-4">
                <div class="form-group">
                    <label>Tanggal verifikasi</label>
                    <input type="date" name="withdraw_verification_date" class="form-control" value="{{ ($withdraw->withdraw_verification_date) ? $withdraw->withdraw_verification_date : date('Y-m-d') }}">
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label>Status</label>
                    <select name="withdraw_status_id" class="form-control">
                        <option value="">-- Pilih Status --</option>
                        @foreach ($withdraw_status as $status)
                            <option value="{{ $status->withdraw_status_id }}" {{ ($withdraw->withdraw_status_id == $status->withdraw_status_id) ? 'selected' : '' }}>{{ $status->withdraw_status_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label>Diambil dari bank perusahaan</label>
                    <select name="withdraw_company_bank_id" id="" class="form-control">
                        <option value="">-- Pilih bank perusahaan --</option>
                        <option value="1" {{ ($withdraw->withdraw_company_bank_id == '1') ? 'selected' : '' }}>BRI  742701002047538 An Taufik Hidayat</option>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success btn-sm">SIMPAN VERIFIKASI</button>
    </div>
</form>

<script>
(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[name="withdraw"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');
        var withdraw_status_id = form.find('select[name="withdraw_status_id"]').val();
        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                toastr.success(res.text);
                call_view_list(1);
            }
        });
        $('#modal-withdraw').modal('hide');
    });
})();
</script>

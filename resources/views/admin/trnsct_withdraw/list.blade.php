<table class="table table-sm table-bordered table-hover mb-3">
    <thead class="bg-info">
        <th class="text-center" width="5%">No</th>
        <th width="15%">Tanggal / Jam</th>
        <th width="15%">ID Transaksi</th>
        <th>Nama</th>
        <th width="15%">Jenis Withdraw</th>
        <th width="12%">Jumlah</th>
        <th width="12%">Status</th>
        <th width="10%">Verifikasi</th>
    </thead>
    <tbody>
        <?php
            if($datas->currentPage() > 1) {
                $current = $datas->currentPage() - 1;
                $no = (15 * $current) + 1;
            } else {
                $no = 1;
            }
        ?>
        @foreach ($datas as $item)
        <tr>
            <td class="text-center">{{ $no }}.</td>
            <td>{{ GeneralHelper::konversiTgl($item->withdraw_date) }}</td>
            <td>{{ $item->withdraw_number }}</td>
            <td>{{ $item->member_name }}</td>
            <td>{{ $item->saving_type_name }}</td>
            <td>{{ GeneralHelper::rupiah($item->withdraw_nominal) }}</td>
            <td>
                @if ($item->withdraw_status_id == '0')
                    <span class="text-secondary">
                        Menuggu verifikasi
                    </span>
                @elseif ($item->withdraw_status_id == '1')
                    <span class="text-green">
                        Berhasil
                    </span>
                @else
                    <span class="text-red">
                        Gagal
                    </span>
                @endif
            </td>
            <td>
                <button type="button" class="btn btn-xs btn-success" onclick="show_form_verification('{{ $item->withdraw_id }}')">
                    <i class="fas fa-check-circle"></i> Verifikasi
                </button>
            </td>
        </tr>
        @php $no = $no + 1 @endphp
        @endforeach
    </tbody>
</table>

@if ($datas->lastPage() > 1)
<ul class="pagination">
    <li class="page-item {{ ($datas->currentPage() == 1) ? ' disabled' : '' }}">
        <a class="page-link" href="javascript:void(0)" onclick="call_view_list(1)">Previous</a>
    </li>
    @for ($i = 1; $i <= $datas->lastPage(); $i++)
        <li class="page-item {{ ($datas->currentPage() == $i) ? ' active' : '' }}">
            <a class="page-link" href="javascript:void(0)" onclick="call_view_list({{ $i }})">{{ $i }}</a>
        </li>
        @endfor
        <li class="page-item {{ ($datas->currentPage() == $datas->lastPage()) ? ' disabled' : '' }}">
            <a class="page-link" href="javascript:void(0)"
                onclick="call_view_list({{ $datas->url($datas->currentPage()+1) }})">Next</a>
        </li>
</ul>
@endif

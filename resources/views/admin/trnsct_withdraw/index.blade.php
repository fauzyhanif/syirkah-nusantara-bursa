@extends('layouts.admin')

@section('content')

<input type="hidden" name="date_start" value="{{ $date_start }}">
<input type="hidden" name="date_end" value="{{ $date_end }}">

<section class="content-header">
    <h1>Withdraw Member</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row mb-4">
            <div class="col-lg-2 sm-mb-2">
                <input type="text" name="withdraw_date" class="form-control form-control-sm">
            </div>
            <div class="col-lg-2 sm-mb-2">
                <select name="withdraw_status_id" class="form-control form-control-sm">
                    <option value="*">-- Semua Status --</option>
                    <option value="0">Menunggu konfirmasi</option>
                    <option value="1">Berhasil</option>
                    <option value="2">Gagal</option>
                </select>
            </div>
            <div class="col-lg-2 sm-mb-2">
                <input type="text" name="member_name" class="form-control form-control-sm" placeholder="Nama Member">
            </div>
            <div class="col-lg-1">
                <button type="button" class="btn btn-sm btn-primary btn-block" onclick="call_view_list();">
                    TAMPILKAN
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-none">
                    <div class="card-body" id="view-list">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('admin.trnsct_withdraw.form')

<script>
$(document).ready(function() {
    var date_start = $('input[name="date_start"]').val();
    var date_end = $('input[name="date_end"]').val();
    $('input[name="withdraw_date"]').daterangepicker({
    startDate: date_start,
    endDate: date_end,
		locale : {
			format : 'YYYY-MM-DD'
		}
    });

    call_view_list(1);
})

function call_view_list(current_page) {
    var withdraw_date = $('input[name="withdraw_date"]').val();
    var withdraw_status_id = $('select[name="withdraw_status_id"]').val();
    var member_name = $('input[name="member_name"]').val();
    var _token = '<?php echo csrf_token() ?>';
    var data = "withdraw_date="+withdraw_date+"&withdraw_status_id="+withdraw_status_id+"&member_name="+member_name+"&_token="+_token;

    $.ajax({
        type: 'POST',
        url: {!! json_encode(url('/admin/withdraw/list?page=')) !!}+current_page,
        dataType:'html',
        data:data,
        success: function(res){
           $('#view-list').html(res)
        }
    });
}

function show_form_verification(withdraw_id) {
    $('#modal-withdraw').modal('show');

    $.ajax({
        type: 'GET',
        url: {!! json_encode(url('/admin/withdraw/view_modal?withdraw_id=')) !!}+withdraw_id,
        dataType:'html',
        success: function(res){
            $('#view-modal').html(res)
        }
    });
}
</script>
@endsection

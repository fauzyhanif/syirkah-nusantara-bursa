<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Bursa Syirkah | Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ url('public/admin-lte/plugins/fontawesome-free/css/all.min.css') }}">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{ url('public/admin-lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ url('public/admin-lte/dist/css/adminlte.min.css') }}">
        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    </head>

    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <img src="{{ url('public/company_file/logo.png') }}" alt="AdminLTE Logo" style="max-width: 150px; ">
                <br>
                <b>Bursa Syirkah</b>
            </div>
            <!-- /.login-logo -->
            <div class="card">
                <div class="card-body login-card-body">
                    <p class="login-box-msg">silakan masukan alamat email anda</p>

                    @error('email')
                    <span class="text-red" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    @error('password')
                    <span class="text-red" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    <form method="POST" action="{{ route('login_custom') }}">
                        @csrf
                        <div class="input-group mb-3">
                            <input type="email" class="form-control" @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary btn-block">MASUK</button>
                            </div>
                        </div>
                    </form>

                    <p class="mt-3 mb-0">
                        Lupa password?
                        <a href="{{ route('password.request') }}">Reset password anda</a>
                    </p>
                    <p class="mb-0">
                        Tidak mempunyai akun?
                        <a href="{{ route('register') }}" class="text-center">Daftar sekarang</a>
                    </p>
                </div>
                <!-- /.login-card-body -->
            </div>
        </div>
        <!-- /.login-box -->

        <!-- jQuery -->
        <script src="{{ url('public/public/admin-lte/plugins/jquery/jquery.min.js') }}"></script>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <!-- AdminLTE App -->
        <script src="{{ url('public/public/admin-lte/dist/js/adminlte.min.js') }}"></script>

    </body>

</html>

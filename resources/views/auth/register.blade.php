<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Bursa Syirkah | Daftar</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ url('public/admin-lte/plugins/fontawesome-free/css/all.min.css') }}">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{ url('public/admin-lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ url('public/admin-lte/dist/css/adminlte.min.css') }}">
        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    </head>

    <body class="hold-transition register-page">
        <div class="register-box">
            <div class="login-logo mt-4">
                <img src="{{ url('public/company_file/logo.png') }}" alt="AdminLTE Logo" style="max-width: 150px; margin-top: 130px">
                <br>
                <b>Bursa Syirkah</b>
            </div>

            <div class="card">
                <div class="card-body register-card-body">
                    <p class="login-box-msg">Registrasi Anggota Baru</p>

                    @error('name')
                    <span class="text-red" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    @error('email')
                    <span class="text-red" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    @error('phone')
                    <span class="text-red" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    @error('password')
                    <span class="text-red" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror



                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="input-group mb-3">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Nama Lengkap">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-user"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Alamat Email">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="name" placeholder="No Telp">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-phone"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password Min. 8 Karakter">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Konfirmasi Password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 mb-3">
                                <button type="submit" class="btn btn-primary btn-block btn-submit" onclick="process()">DAFTAR</button>
                            </div>

                            <div class="col-lg-12">
                                <div class="alert alert-info">
                                    Kami akan mengirimkan pesan ke alamat email anda, mungkin membutuhkan beberapa menit sampai pesan diterima
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>

                    <br>
                    Sudah mempunyai akun?
                    <a href="{{ route('login') }}" class="text-center mt-4">Login Sekarang</a>
                </div>
                <!-- /.form-box -->
            </div><!-- /.card -->
        </div>
        <!-- /.register-box -->



        <!-- jQuery -->
        <script src="{{ url('public/public/admin-lte/plugins/jquery/jquery.min.js') }}"></script>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <!-- AdminLTE App -->
        <script src="{{ url('public/public/admin-lte/dist/js/adminlte.min.js') }}"></script>

        <script>
            function process() {
                $('.btn-submit').html("Proses...")
            }
        </script>
    </body>

</html>

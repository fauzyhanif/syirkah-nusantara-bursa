<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (!Session::has('name')) {
        return redirect('/login');
    }
});

Auth::routes(['verify' => true]);
Route::post('custom_registration', 'Auth\RegisterController@customRegistration')->name('custom_registration');

Route::post('/login_custom', 'Auth\LoginController@loginCustom')->name('login_custom');

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
Route::get('/notification_get_data', 'HomeController@notificationGetData')->name('notification_get_data')->middleware('auth');
Route::get('/notification_update_read', 'HomeController@notificationUpdateRead')->name('notification_update_read')->middleware('auth');
Route::get('/conform_password_success', 'HomeController@confirmPasswordSuccess')->name('conform_password_success')->middleware('verified');

// admin
Route::get('/admin/dashboard', 'AdminDashboardController@index')->name('admin.dashboard')->middleware('admin');
Route::get('/admin/member', 'AdminMemberController@index')->name('admin.member')->middleware('admin');
Route::post('/admin/member/list', 'AdminMemberController@list')->name('admin.member.list')->middleware('admin');
Route::get('/admin/member/detail/{id}', 'AdminMemberController@detail')->name('admin.member.detail')->middleware('admin');
Route::get('/admin/member/saving_list/{id}', 'AdminMemberController@savingList')->name('admin.member.saving_list')->middleware('admin');
Route::get('/admin/member/list_topup/{id}', 'AdminMemberController@listTopup')->name('admin.member.list_topup')->middleware('admin');
Route::get('/admin/member/list_withdraw/{id}', 'AdminMemberController@listWithdraw')->name('admin.member.list_withdraw')->middleware('admin');
Route::post('/admin/member/update_status/{id}', 'AdminMemberController@updateStatus')->name('admin.member.update_status')->middleware('admin');

Route::get('/admin/topup', 'AdminTopupController@index')->name('admin.topup')->middleware('admin');
Route::post('/admin/topup/list', 'AdminTopupController@list')->name('admin.topup.list')->middleware('admin');
Route::get('/admin/topup/view_modal', 'AdminTopupController@viewModal')->name('admin.topup.view_modal')->middleware('admin');
Route::post('/admin/topup/verification/{id}', 'AdminTopupController@verification')->name('admin.topup.verification')->middleware('admin');

Route::get('/admin/withdraw', 'AdminWithdrawController@index')->name('admin.withdraw')->middleware('admin');
Route::post('/admin/withdraw/list', 'AdminWithdrawController@list')->name('admin.withdraw.list')->middleware('admin');
Route::get('/admin/withdraw/view_modal', 'AdminWithdrawController@viewModal')->name('admin.withdraw.view_modal')->middleware('admin');
Route::post('/admin/withdraw/verification/{id}', 'AdminWithdrawController@verification')->name('admin.withdraw.verification')->middleware('admin');

// member
Route::get('/member/dashboard', 'MemberDashboardController@index')->name('member.dashboard')->middleware(['verified', 'member']);
Route::get('/member/dashboard/saving_list', 'MemberDashboardController@savingList')->name('member.dashboard.saving_list')->middleware(['verified', 'member']);

Route::get('/member/verifikasi', 'MemberVerifikasiController@index')->name('member.verifikasi')->middleware(['verified', 'member']);
Route::get('/member/prevent/identity', 'MemberVerifikasiController@preventIdentity')->name('member.prevent.identity')->middleware(['verified', 'member']);

Route::get('/member/profile', 'MemberProfileController@index')->name('member.profile')->middleware(['verified', 'member']);
Route::get('/member/profile_form', 'MemberProfileController@form')->name('member.profile_form')->middleware(['verified', 'member']);
Route::post('/member/profile_update', 'MemberProfileController@update')->name('member.profile_update')->middleware(['verified', 'member']);

Route::get('/member/topup/list', 'MemberTopupController@list')->name('member.topup.list')->middleware(['verified', 'member']);
Route::get('/member/topup/form_store', 'MemberTopupController@formStore')->name('member.topup.form_store')->middleware(['verified', 'member']);
Route::post('/member/topup/store', 'MemberTopupController@store')->name('member.topup.store')->middleware(['verified', 'member']);
Route::get('/member/topup/detail/{id}', 'MemberTopupController@detail')->name('member.topup.detail')->middleware(['verified', 'member']);
Route::get('/member/topup/how_to_pay/{id}', 'MemberTopupController@howToPay')->name('member.topup.how_to_pay')->middleware(['verified', 'member']);
Route::get('/member/topup/form_update/{id}', 'MemberTopupController@formUpdate')->name('member.topup.form_update')->middleware(['verified', 'member']);
Route::post('/member/topup/update/{id}', 'MemberTopupController@update')->name('member.topup.update')->middleware(['verified', 'member']);
Route::get('/member/topup/delete/{id}', 'MemberTopupController@delete')->name('member.topup.delete')->middleware(['verified', 'member']);
Route::get('/member/topup/transfered/{id}', 'MemberTopupController@transfered')->name('member.topup.transfered')->middleware(['verified', 'member']);

Route::get('/member/withdraw/list', 'MemberWithdrawController@list')->name('member.withdraw.list')->middleware(['verified', 'member']);
Route::get('/member/withdraw/form_store', 'MemberWithdrawController@formStore')->name('member.withdraw.form_store')->middleware(['verified', 'member']);
Route::post('/member/withdraw/store', 'MemberWithdrawController@store')->name('member.withdraw.store')->middleware(['verified', 'member']);
Route::get('/member/withdraw/detail/{id}', 'MemberWithdrawController@detail')->name('member.withdraw.detail')->middleware(['verified', 'member']);
Route::get('/member/withdraw/how_to_pay/{id}', 'MemberWithdrawController@howToPay')->name('member.withdraw.how_to_pay')->middleware(['verified', 'member']);
Route::get('/member/withdraw/form_update/{id}', 'MemberWithdrawController@formUpdate')->name('member.withdraw.form_update')->middleware(['verified', 'member']);
Route::post('/member/withdraw/update/{id}', 'MemberWithdrawController@update')->name('member.withdraw.update')->middleware(['verified', 'member']);
Route::get('/member/withdraw/delete/{id}', 'MemberWithdrawController@delete')->name('member.withdraw.delete')->middleware(['verified', 'member']);

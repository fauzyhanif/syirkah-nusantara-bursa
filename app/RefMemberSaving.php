<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefMemberSAving extends Model
{
    protected $table = "ref_member_saving";
    protected $guarded = [];

    public function saving_types()
    {
        return $this->belongsTo(RefSavingType::class, 'saving_type', 'saving_type_id');
    }
}

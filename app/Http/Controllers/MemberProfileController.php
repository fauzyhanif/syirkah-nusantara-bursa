<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\RefMembers;
use App\RefIncome;
use App\RefJobs;
use App\RefMaritalStatus;

class MemberProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $member = RefMembers::with(['job', 'marital_status'])->where("member_id", "$user_id")->first();
        return view('member.profile.index', compact('member'));
    }

    public function form()
    {
        $user_id = Auth::user()->id;
        $member = RefMembers::where("member_id", "$user_id")->first();
        $jobs = RefJobs::all();
        $marital_status = RefMaritalStatus::all();

        return view('member.profile.form', \compact('member', 'jobs', 'marital_status'));
    }

    public function update(Request $request)
    {
        $user_id = Auth::user()->id;
        $member = RefMembers::where("member_id", "$user_id")->first();
        $member_file_ktp = $member->member_file_ktp;
        $member_file_selfie = $member->member_file_selfie;
        $member_file_npwp = $member->member_file_npwp;

        if ($request->file('member_file_ktp')) {
            $file = $request->file('member_file_ktp');
            $member_file_ktp = $user_id . "_ktp." . $file->getClientOriginalExtension();
            $file->move('member_files', $member_file_ktp);
        }

        if ($request->file('member_file_selfie')) {
            $file = $request->file('member_file_selfie');
            $member_file_selfie = $user_id . "_selfie." . $file->getClientOriginalExtension();
            $file->move('member_files', $member_file_selfie);
        }

        if ($request->file('member_file_npwp')) {
            $file = $request->file('member_file_npwp');
            $member_file_npwp = $user_id . "_npwp." . $file->getClientOriginalExtension();
            $file->move('member_files', $member_file_npwp);
        }

        $member = RefMembers::where("member_id", "$user_id")->update([
            "member_name" => $request->get('member_name'),
            "member_phone" => $request->get('member_phone'),
            "member_nik" => $request->get('member_nik'),
            "member_npwp" => $request->get('member_npwp'),
            "member_job_id" => $request->get('member_job_id'),
            "member_income" => str_replace(".", "", $request->get('member_income')),
            "member_income_source" => $request->get('member_income_source'),
            "member_place_of_birth" => $request->get('member_place_of_birth'),
            "member_date_of_birth" => $request->get('member_date_of_birth'),
            "member_marital_status_id" => $request->get('member_marital_status_id'),
            "member_couple" => $request->get('member_couple'),
            "member_mother" => $request->get('member_mother'),
            "member_address" => $request->get('member_address'),
            "member_file_ktp" => "$member_file_ktp",
            "member_file_selfie" => "$member_file_selfie",
            "member_file_npwp" => "$member_file_npwp",
        ]);

        $res = [
            "status" => "success",
            "title" => "Berhasil",
            "text" => "Data pribadi berhasil diperbaharui"
        ];

        return response()->json($res);
    }
}

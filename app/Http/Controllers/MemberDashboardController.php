<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\RefMembers;
use App\RefMemberSaving;

class MemberDashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $member = RefMembers::where("member_id", "$user_id")->first();
        return view('member.dashboard.index', compact('member'));
    }

    public function savingList()
    {
        $user_id = Auth::user()->id;
        $savings = RefMemberSaving::with('saving_types')->where("member_id", "$user_id")->get();
        return view('member.dashboard.saving_list', compact('savings'));
    }
}

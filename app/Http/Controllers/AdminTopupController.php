<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\RefMembers;
use App\RefMemberSaving;
use App\TrnsctTopup;
use App\RefTopupStatus;

class AdminTopupController extends Controller
{
    public function index()
    {
        $date_start = date('Y-m-01');
        $date_end = date('Y-m-d');

        // update notification read
        TrnsctTopup::where("topup_notification_read", "0")->update(["topup_notification_read" => "1"]);

        return view('admin.trnsct_topup.index', compact('date_start', 'date_end'));
    }

    public function list(Request $request)
    {
        $topup_date = $request->get('topup_date');
        $date_start = explode(" - ", $topup_date)[0];
        $date_end = explode(" - ", $topup_date)[1];
        $topup_status_id = $request->get('topup_status_id');
        $member_name = $request->get('member_name');

        $param_topup_status_id = ($topup_status_id == '*') ? '' : "AND trnsct_topup.topup_status_id = '$topup_status_id'";
        $param_member_name = ($member_name == '') ? '' : "AND ref_members.member_name like '%$member_name%'";

        $datas = DB::table('trnsct_topup')
                ->select("trnsct_topup.*", "ref_members.*", "ref_saving_type.*")
                ->leftJoin("ref_members", "trnsct_topup.topup_member_id", "=", "ref_members.member_id")
                ->leftJoin("ref_saving_type", "trnsct_topup.saving_type_id", "=", "ref_saving_type.saving_type_id")
                ->whereRaw("(DATE(trnsct_topup.topup_date) BETWEEN '$date_start' AND '$date_end') $param_topup_status_id $param_member_name")
                ->orderBy("trnsct_topup.topup_date", "DESC")
                ->paginate(15);

        return view('admin.trnsct_topup.list', compact('datas'));
    }

    public function viewModal(Request $request)
    {
        $topup_id = $request->get('topup_id');
        $topup = TrnsctTopup::with(['topup_type', 'topup_status', 'topup_member'])->where("topup_id", "$topup_id")->first();
        $topup_status = RefTopupStatus::where("topup_status_id", "!=", "0")->get();

        return view('admin.trnsct_topup.view_modal', compact('topup', 'topup_status'));
    }

    public function verification(Request $request, $topup_id)
    {
        $member_id = $request->get('topup_member_id');
        $topup = TrnsctTopup::where("topup_id", "$topup_id")->update([
            "topup_status_id" => $request->get('topup_status_id'),
            "topup_verification_date" => $request->get('topup_verification_date'),
            "topup_company_bank_id" => $request->get('topup_company_bank_id'),
        ]);

        $res = [
            "status" => "success",
            "title" => "Berhasil",
            "text" => "Verifikasi data topup berhasil"
        ];

        return response()->json($res);
    }

}

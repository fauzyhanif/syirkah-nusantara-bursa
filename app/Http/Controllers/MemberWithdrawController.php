<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RefSavingType;
use Illuminate\Support\Facades\Auth;
use App\TrnsctWithdraw;
use App\RefTransactionNumber;
use App\RefMembers;
use App\RefMemberSaving;

class MemberWithdrawController extends Controller
{
    public function list()
    {
        $user_id = Auth::user()->id;
        $withdraws = TrnsctWithdraw::with('withdraw_status')->where("withdraw_member_id", "$user_id")->orderBy("withdraw_number", "desc")->limit(10)->get();
        return view('member.withdraw.list', compact("withdraws"));
    }

    public function detail($withdraw_id)
    {
        $withdraw = TrnsctWithdraw::with('withdraw_type')->where("withdraw_id", "$withdraw_id")->first();
        return view('member.withdraw.detail', compact("withdraw"));
    }

    public function formStore()
    {
        $user_id = Auth::user()->id;
        $member = RefMembers::where('member_id', "$user_id")->first();

        if ($member->member_verified == '0') {
            return redirect('member/prevent/identity');
        }

        $saving_types = RefSavingType::where("saving_type_id", "!=", "1")->get();
        return view('member.withdraw.form_store', compact("saving_types"));
    }

    public function store(Request $request)
    {
        // cek apakah punya tabungan atau tidak
        $saving = RefMemberSaving::where("member_id", Auth::user()->id)->where("saving_type", $request->get('saving_type_id'))->first();

        if (($saving->amount - $saving->amount_withdrawn) >= (int) str_replace(".", "", $request->get('withdraw_nominal'))) {
            $withdraw_number = "";
            $new = new \App\TrnsctWithdraw();
            $new->withdraw_number = $this->getWithdrawNumber();
            $new->withdraw_date = date('Y-m-d H:i:s');
            $new->withdraw_member_id = Auth::user()->id;
            $new->saving_type_id = $request->get('saving_type_id');
            $new->withdraw_nominal = str_replace(".", "", $request->get('withdraw_nominal'));
            $new->withdraw_member_bank = $request->get('withdraw_member_bank');
            $new->withdraw_member_rek_number = $request->get('withdraw_member_rek_number');

            if ($new->save()) {
                $res = [
                    "status" => "success",
                    "title" => "Berhasil",
                    "text" => "Input withdraw berhasil.",
                    "withdraw_id" => $new->id
                ];
            } else {
                $res = [
                    "status" => "error",
                    "title" => "Gagal",
                    "text" => "Input withdraw gagal."
                ];
            }
        } else {
            $res = [
                "status" => "error",
                "title" => "Gagal",
                "text" => "Saldo anda kurang dari nominal withdraw ."
            ];
        }

        return response()->json($res);
    }

    public function formUpdate($withdraw_id)
    {
        $withdraw = TrnsctWithdraw::where("withdraw_id", "$withdraw_id")->first();
        $saving_types = RefSavingType::where("saving_type_id", "!=", "3")->get();
        return view('member.withdraw.form_update', compact("withdraw", "saving_types"));
    }

    public function update(Request $request, $withdraw_id)
    {
        // cek apakah punya tabungan atau tidak
        $saving = RefMemberSaving::where("member_id", Auth::user()->id)->where("saving_type", $request->get('saving_type_id'))->first();

        if (($saving->amount - $saving->amount_withdrawn) >= (int) str_replace(".", "", $request->get('withdraw_nominal'))) {
            TrnsctWithdraw::where("withdraw_id", "$withdraw_id")->update([
                "saving_type_id" => $request->get('saving_type_id'),
                "withdraw_nominal" => str_replace(".", "", $request->get('withdraw_nominal')),
                "withdraw_member_bank" => $request->get('withdraw_member_bank'),
                "withdraw_member_rek_number" => $request->get('withdraw_member_rek_number')
            ]);

            $res = [
                "status" => "success",
                "title" => "Berhasil",
                "text" => "Ubah withdraw berhasil.",
                "withdraw_id" => $withdraw_id
            ];
        } else {
            $res = [
                "status" => "error",
                "title" => "Gagal",
                "text" => "Saldo anda kurang dari nominal withdraw ."
            ];
        }

        return response()->json($res);
    }

    public function delete($withdraw_id)
    {
        TrnsctWithdraw::where("withdraw_id", "$withdraw_id")->delete();

        $res = [
            "status" => "success",
            "title" => "Berhasil",
            "text" => "Ubah withdraw berhasil.",
            "withdraw_id" => $withdraw_id
        ];

        return redirect("member/dashboard");
    }

    public function getWithdrawNumber()
    {
        $this_month = date('Y-m');
        $withdraw_number = 'WD-';

        $last_number = \App\RefTransactionNumber::where('trnsct_month', '=', "$this_month")
            ->where('trnsct_type', "WITHDRAW")
            ->limit(1)
            ->get();

        // check exist or not
        if ( count($last_number) == 0 ) {
            $order_id = "00001";

            $new = new RefTransactionNumber();
            $new->trnsct_type = "WITHDRAW";
            $new->trnsct_month = "$this_month";
            $new->trnsct_serial_num = "00002";
            $new->save();
        } else {
            $order_id = sprintf('%05d', $last_number[0]->trnsct_serial_num);
            $update = RefTransactionNumber::where('trnsct_month', '=', "$this_month")
                ->where("trnsct_type", "WITHDRAW")
                ->update(["trnsct_serial_num" => $order_id + 1]);
        }

        $withdraw_number .= date('Ym') . $order_id;
        return $withdraw_number;
    }
}

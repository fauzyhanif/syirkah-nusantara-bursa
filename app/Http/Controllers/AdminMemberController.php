<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\RefMembers;
use App\RefMemberSaving;
use App\TrnsctTopup;
use App\TrnsctWithdraw;

class AdminMemberController extends Controller
{
    public function index()
    {
        return view('admin.ref_member.index');
    }

    public function list(Request $request)
    {
        $member_verified = $request->get('member_verified');
        $member_actived = $request->get('member_actived');
        $member_name = $request->get('member_name');

        $param_member_verified = ($member_verified == '*') ? '' : "AND member_verified = '$member_verified'";
        $param_member_actived = ($member_actived == '*') ? '' : "AND member_actived = '$member_actived'";
        $param_member_name = ($member_name == '') ? '' : "AND member_name like '%$member_name%'";

        $datas = DB::table('ref_members')->join('users', 'ref_members.member_id', '=', 'users.id')->select('ref_members.*', 'users.email')->whereRaw("member_id != '' $param_member_verified $param_member_actived $param_member_name")->paginate(15);
        return view('admin.ref_member.list', compact('datas'));
    }

    public function detail($member_id)
    {
        $member = RefMembers::with(['job', 'marital_status', 'user'])->where("member_id", "$member_id")->first();
        return view('admin.ref_member.detail', \compact('member'));
    }

    public function listTopup($member_id)
    {
        $topups = TrnsctTopup::with(['topup_status', 'topup_type'])->where("topup_member_id", "$member_id")->orderBy("topup_number", "desc")->paginate(15);
        return view('admin.ref_member.list_topup', compact("topups"));
    }

    public function listWithdraw($member_id)
    {
        $withdraws = TrnsctWithdraw::with(['withdraw_status', 'withdraw_type'])->where("withdraw_member_id", "$member_id")->orderBy("withdraw_number", "desc")->paginate(15);
        return view('admin.ref_member.list_withdraw', compact("withdraws"));
    }

    public function savingList($member_id)
    {
        $savings = RefMemberSaving::with('saving_types')->where("member_id", "$member_id")->get();
        return view('member.dashboard.saving_list', compact('savings'));
    }

    public function updateStatus(Request $request, $member_id)
    {
        RefMembers::where("member_id", "$member_id")->update([
            "member_verified" => $request->get('member_verified'),
            "member_actived" => $request->get('member_actived'),
        ]);

        $res = [
            "status" => "success",
            "title" => "Berhasil",
            "text" => "Verifikasi data member berhasil"
        ];

        return response()->json($res);
    }

    public function verifikasi()
    {
        return view('admin.ref_member.verifikasi');
    }
}

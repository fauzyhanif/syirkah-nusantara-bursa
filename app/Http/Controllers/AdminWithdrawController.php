<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\RefMembers;
use App\RefMemberSaving;
use App\TrnsctWithdraw;
use App\RefWithdrawStatus;

class AdminWithdrawController extends Controller
{
    public function index()
    {
        $date_start = date('Y-m-01');
        $date_end = date('Y-m-d');

        // update notification read
        TrnsctWithdraw::where("withdraw_notification_read", "0")->update(["withdraw_notification_read" => "1"]);

        return view('admin.trnsct_withdraw.index', compact('date_start', 'date_end'));
    }

    public function list(Request $request)
    {
        $withdraw_date = $request->get('withdraw_date');
        $date_start = explode(" - ", $withdraw_date)[0];
        $date_end = explode(" - ", $withdraw_date)[1];
        $withdraw_status_id = $request->get('withdraw_status_id');
        $member_name = $request->get('member_name');

        $param_withdraw_status_id = ($withdraw_status_id == '*') ? '' : "AND trnsct_withdraw.withdraw_status_id = '$withdraw_status_id'";
        $param_member_name = ($member_name == '') ? '' : "AND ref_members.member_name like '%$member_name%'";

        $datas = DB::table('trnsct_withdraw')
                ->select("trnsct_withdraw.*", "ref_members.*", "ref_saving_type.*")
                ->leftJoin("ref_members", "trnsct_withdraw.withdraw_member_id", "=", "ref_members.member_id")
                ->leftJoin("ref_saving_type", "trnsct_withdraw.saving_type_id", "=", "ref_saving_type.saving_type_id")
                ->whereRaw("(DATE(trnsct_withdraw.withdraw_date) BETWEEN '$date_start' AND '$date_end') $param_withdraw_status_id $param_member_name")
                ->paginate(15);

        return view('admin.trnsct_withdraw.list', compact('datas'));
    }

    public function viewModal(Request $request)
    {
        $withdraw_id = $request->get('withdraw_id');
        $withdraw = Trnsctwithdraw::with(['withdraw_type', 'withdraw_status', 'withdraw_member'])->where("withdraw_id", "$withdraw_id")->first();
        $withdraw_status = RefWithdrawStatus::all();

        return view('admin.trnsct_withdraw.view_modal', compact('withdraw', 'withdraw_status'));
    }

    public function verification(Request $request, $withdraw_id)
    {
        $member_id = $request->get('withdraw_member_id');
        $withdraw = Trnsctwithdraw::where("withdraw_id", "$withdraw_id")->update([
            "withdraw_status_id" => $request->get('withdraw_status_id'),
            "withdraw_verification_date" => $request->get('withdraw_verification_date'),
            "withdraw_company_bank_id" => $request->get('withdraw_company_bank_id'),
        ]);

        $res = [
            "status" => "success",
            "title" => "Berhasil",
            "text" => "Verifikasi data withdraw berhasil"
        ];

        return response()->json($res);
    }

}

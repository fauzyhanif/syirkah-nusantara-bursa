<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\RefSavingType;
use App\TrnsctTopup;
use App\RefMembers;
use App\RefTransactionNumber;

class MemberTopupController extends Controller
{
    public function list()
    {
        $user_id = Auth::user()->id;
        $topups = TrnsctTopup::with('topup_status')->where("topup_member_id", "$user_id")->orderBy("topup_number", "desc")->limit(10)->get();
        return view('member.topup.list', compact("topups"));
    }

    public function detail($topup_id)
    {
        $topup = TrnsctTopup::with('topup_type')->where("topup_id", "$topup_id")->first();
        return view('member.topup.how_to_pay', compact("topup"));
    }

    public function formStore()
    {
        $saving_types = RefSavingType::where("saving_type_id", "!=", "3")->get();
        return view('member.topup.form_store', compact("saving_types"));
    }

    public function store(Request $request)
    {
        $topup_unique_code = rand(1,999);

        $topup_number = "";
        $new = new \App\TrnsctTopup();
        $new->topup_number = $this->getTopupNumber();
        $new->topup_date = date('Y-m-d H:i');
        $new->topup_expired = date('Y-m-d', strtotime(' +1 day')) . " " . date('H:i');
        $new->topup_member_id = Auth::user()->id;
        $new->saving_type_id = $request->get('saving_type_id');
        $new->topup_nominal = str_replace(".", "", $request->get('topup_nominal')) + $topup_unique_code;
        $new->topup_unique_code = $topup_unique_code;
        $new->topup_member_bank = $request->get('topup_member_bank');
        $new->topup_member_rek_number = $request->get('topup_member_rek_number');

        if ($new->save()) {
            $res = [
                "status" => "success",
                "title" => "Berhasil",
                "text" => "Input topup berhasil.",
                "topup_id" => $new->id
            ];
        } else {
            $res = [
                "status" => "error",
                "title" => "Gagal",
                "text" => "Input topup gagal."
            ];
        }

        return response()->json($res);
    }

    public function formUpdate($topup_id)
    {
        $topup = TrnsctTopup::where("topup_id", "$topup_id")->first();
        $saving_types = RefSavingType::where("saving_type_id", "!=", "3")->get();
        return view('member.topup.form_update', compact("topup", "saving_types"));
    }

    public function update(Request $request, $topup_id)
    {
        TrnsctTopup::where("topup_id", "$topup_id")->update([
            "saving_type_id" => $request->get('saving_type_id'),
            "topup_nominal" => str_replace(".", "", $request->get('topup_nominal')),
            "topup_member_bank" => $request->get('topup_member_bank'),
            "topup_member_rek_number" => $request->get('topup_member_rek_number')
        ]);

        $res = [
            "status" => "success",
            "title" => "Berhasil",
            "text" => "Ubah topup berhasil.",
            "topup_id" => $topup_id
        ];

        return response()->json($res);
    }

    public function delete($topup_id)
    {
        TrnsctTopup::where("topup_id", "$topup_id")->delete();

        $res = [
            "status" => "success",
            "title" => "Berhasil",
            "text" => "Ubah topup berhasil.",
            "topup_id" => $topup_id
        ];

        return redirect("member/dashboard");
    }

    public function transfered($topup_id)
    {
        $topup = TrnsctTopup::where("topup_id", $topup_id)->first();

        if ($topup->topup_status_id == '0') {
            TrnsctTopup::where("topup_id", $topup_id)->update(["topup_status_id" => "3"]);
        }

        return redirect()->back();
    }

    public function getTopupNumber()
    {
        $this_month = date('Y-m');
        $topup_number = 'TU-';

        $last_number = \App\RefTransactionNumber::where('trnsct_month', '=', "$this_month")
            ->where('trnsct_type', "TOPUP")
            ->limit(1)
            ->get();

        // check exist or not
        if ( count($last_number) == 0 ) {
            $order_id = "00001";

            $new = new RefTransactionNumber();
            $new->trnsct_type = "TOPUP";
            $new->trnsct_month = "$this_month";
            $new->trnsct_serial_num = "00002";
            $new->save();
        } else {
            $order_id = sprintf('%05d', $last_number[0]->trnsct_serial_num);
            $update = RefTransactionNumber::where('trnsct_month', '=', "$this_month")
                ->where("trnsct_type", "TOPUP")
                ->update(["trnsct_serial_num" => $order_id + 1]);
        }

        $topup_number .= date('Ym') . $order_id;
        return $topup_number;
    }

    public function howToPay($topup_id)
    {
        $topup = TrnsctTopup::where("topup_id", "$topup_id")->first();
        return view('member.topup.how_to_pay', compact('topup'));
    }
}

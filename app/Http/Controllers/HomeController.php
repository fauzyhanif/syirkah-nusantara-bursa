<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\RefMembers;
use App\TrnsctTopup;
use App\TrnsctWithdraw;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $member = RefMembers::where("member_id", "$user_id")->first();

        $check_data_has_filled = ($member->member_nik != '') ? true : false;
        return view('home', compact('check_data_has_filled'));
    }

    public function confirmPasswordSuccess()
    {
        return view('confirm_password_success');
    }

    public function notificationGetData()
    {
        $res = [
            "notification_new" => 0,
            "notification_topup" => 0,
            "notification_withdraw" => 0,
        ];

        $topups = TrnsctTopup::where("topup_notification_read", "3")->count();
        $withdraws = TrnsctWithdraw::where("withdraw_notification_read", "0")->count();

        $res['notification_new'] = $topups + $withdraws;
        $res['notification_topup'] = $topups;
        $res['notification_withdraw'] = $withdraws;

        return response()->json($res);
    }

    public function notificationUpdateRead()
    {
        TrnsctTopup::where("topup_notification_read", "0")->update(["topup_notification_read" => "1"]);
        TrnsctWithdraw::where("withdraw_notification_read", "0")->update(["withdraw_notification_read" => "1"]);

        return response()->json(true);
    }
}

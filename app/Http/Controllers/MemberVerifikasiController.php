<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\RefMembers;

class MemberverifikasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $member = RefMembers::where("member_id", "$user_id")->first();
        return view('member.verifikasi.index', compact('member'));
    }

    public function preventIdentity()
    {
        return view('member.prevent.identity');
    }
}

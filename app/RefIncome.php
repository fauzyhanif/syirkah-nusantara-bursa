<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefIncome extends Model
{
    protected $table = "ref_income";
    protected $guarded = [];
}

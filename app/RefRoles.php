<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefRoles extends Model
{
    protected $table = "ref_roles";
    protected $guarded = [];
}

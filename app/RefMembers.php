<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefMembers extends Model
{
    protected $table = "ref_members";
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'member_id', 'id');
    }

    public function job()
    {
        return $this->belongsTo(RefJobs::class, 'member_job_id', 'id');
    }

    public function marital_status()
    {
        return $this->belongsTo(RefMaritalStatus::class, 'member_marital_status_id', 'id');
    }
}

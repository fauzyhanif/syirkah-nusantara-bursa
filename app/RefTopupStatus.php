<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefTopupStatus extends Model
{
    protected $table = "ref_topup_status";
    protected $guarded = [];
}

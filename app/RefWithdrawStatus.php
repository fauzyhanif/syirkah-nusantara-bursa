<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefWithdrawStatus extends Model
{
    protected $table = "ref_withdraw_status";
    protected $guarded = [];
}

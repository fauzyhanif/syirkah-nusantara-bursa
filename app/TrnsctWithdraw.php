<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RefWithdrawStatus;
use App\RefSavingType;

class TrnsctWithdraw extends Model
{
    protected $table = "trnsct_withdraw";
    protected $guarded = [];

    public function withdraw_member()
    {
        return $this->belongsTo(RefMembers::class, 'withdraw_member_id', 'member_id');
    }

    public function withdraw_status()
    {
        return $this->belongsTo(RefWithdrawStatus::class, 'withdraw_status_id', 'withdraw_status_id');
    }

    public function withdraw_type()
    {
        return $this->belongsTo(RefSavingType::class, 'saving_type_id', 'saving_type_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefJobs extends Model
{
    protected $table = "ref_jobs";
    protected $guarded = [];
}

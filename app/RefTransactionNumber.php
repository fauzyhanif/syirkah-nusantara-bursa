<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefTransactionNumber extends Model
{
    protected $table = "ref_transaction_number";
    protected $guarded = [];
}

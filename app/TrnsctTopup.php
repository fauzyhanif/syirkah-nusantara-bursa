<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RefTopupStatus;
use App\RefSavingType;

class TrnsctTopup extends Model
{
    protected $table = "trnsct_topup";
    protected $primayKey = "topup_id";
    protected $guarded = [];

    public function topup_member()
    {
        return $this->belongsTo(RefMembers::class, 'topup_member_id', 'member_id');
    }

    public function topup_status()
    {
        return $this->belongsTo(RefTopupStatus::class, 'topup_status_id', 'topup_status_id');
    }

    public function topup_type()
    {
        return $this->belongsTo(RefSavingType::class, 'saving_type_id', 'saving_type_id');
    }
}

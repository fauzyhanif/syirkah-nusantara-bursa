<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefMaritalStatus extends Model
{
    protected $table = "ref_marital_status";
    protected $guarded = [];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefSavingType extends Model
{
    protected $table = "ref_saving_type";
    protected $guarded = [];
}
